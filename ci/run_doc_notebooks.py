# -*- coding: utf-8 -*-
import io
import os

import nbformat
from nbconvert.preprocessors import ExecutePreprocessor

root_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
nb_path = os.path.join(root_dir, "doc", "notebooks")

list_nb_files = [file_n for file_n in os.listdir(nb_path) if file_n.endswith(".ipynb")]


def run_nb():
    for nb_file in list_nb_files:
        with io.open(os.path.join(nb_path, nb_file), encoding="utf-8") as f:
            nb = nbformat.read(f, as_version=4)
        ep = ExecutePreprocessor(timeout=600, kernel_name="python2")
        ep.preprocess(nb, {"metadata": {"path": nb_path}})

        with io.open(os.path.join(nb_path, nb_file), "wt", encoding="utf-8") as f:
            nbformat.write(nb, f)


if __name__ == "__main__":
    run_nb()
