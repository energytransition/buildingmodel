
Installation
============
Below we describe the recommended installation procedure which relies on using the Conda package manager for consistency
across platforms.

Pre-requisites
--------------

* [Anaconda 3.8 distribution](https://www.anaconda.com/distribution/) or [Miniconda3 distribution](https://docs.conda.io/en/latest/miniconda.html)
* To clone buildingmodel's Gitlab repository, [Git](https://git-scm.com/downloads) (On Windows, [Git for Windows](https://git-for-windows.github.io/) is preferred)
* If you wish to install from source, the cpp compiler corresponding to buildingmodel's python version is needed. On linux,
  ``sudo apt-get install -y gcc make g++``. On windows, follow the instructions at W[indowsCompiler](https://wiki.python.org/moin/WindowsCompilers)
  
Installing from source
----------------------

### Obtaining the source code

You can either :
    - directly download the source in a compressed folder from buildingmodel's main gitlab page : [](https://gitlab.com/energytransition/buildingmodel)
    - clone buildingmodel's repository using git either with SSH
      `git clone git@gitlab.com:energytransition/buildingmodel.git` or with HTTPS
      `git clone https://gitlab.com/energytransition/buildingmodel.git` after having set up the corresponding
      authentication.

### Creating the conda environment

Open a command line tool in the buildingmodel root folder, enter the ``ci`` folder, and then run the following :

```
conda env create -f conda_env.yml
```


For more information on conda environments, please visit https://conda.io/docs/using/envs.html .


### Installing buildingmodel


Activate the environment we have just created :

```
conda activate buildingmodel
```

Then, from the root folder of buildingmodel, run :

```
python setup.py install
```

### Running the test suite

From the root folder of buildingmodel, run :

```
pytest
```

### Obtaining complete census and diagnosis data

BuildingModel source code only contains sample census, district and diagnosis data necessary for testing purposes. To
obtain complete data, download them at :

- [census data](https://storage.googleapis.com/building-inference-data/district_level_census_latest.hdf) to copy in buildingmodel/data/census/district_level_census.hdf
- [district data](https://storage.googleapis.com/building-inference-data/districts_latest.gpkg) to copy in buildingmodel/data/gis/districts.gpkg
- [diagnosis data](https://storage.googleapis.com/building-inference-data/district_level_diagnosis_data_latest.hdf) to copy in buildingmodel/data/diagnosis/district_level_diagnosis_data.hdf

### Obtaining climate data

BuildingModel relies on weather data designed for building thermal energy simulation produced by [onebuilding](http://climate.onebuilding.org/).
Users can download relevant weather data for their use case and copy it into buildingmodel/data/weather.


### Compiling a local version of the documentation


Additional dependencies are needed to compile the documentation :
    - pandoc and graphviz. Can be installed on linux with `sudo apt-get install pandoc graphviz graphviz-dev`. On
      windows, see installation instructions on their respective websites.
    - python packages listed in doc/requirements.txt

When the dependencies are installed, cd into the `doc` folder, and then run the following :

```
make html
```

The resulting documentation can be accessed by opening the file `doc\_build\html\index.html`
