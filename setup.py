from setuptools import setup
from Cython.Build import cythonize
from setuptools import Extension
import numpy as np

extensions = [
    Extension(
        "buildingmodel.cython_utils.shading",
        ["buildingmodel/cython_utils/shading.pyx"],
        include_dirs=[np.get_include()],
    )
]

compiler_directives = {
    "language_level": 3,
    "boundscheck": False,
    "initializedcheck": False,
    "nonecheck": False,
    "cdivision": True,
}


setup(
    ext_modules=cythonize(
        extensions, annotate=True, compiler_directives=compiler_directives
    ),
)
