# -*- coding: utf-8 -*-
import datetime
import pandas as pd
import polars as pl

pl.enable_string_cache()
import geopandas as gpd
from shapely.geometry import Point
from pathlib import Path
from .io.climate import load_data as load_climate_data
from .io.gis import load_data as load_gis_data
from .io.census import load_census_in_case
from .io.diagnosis import load_diagnosis_data
from .io.imbalance_correction import (
    correct_dwelling_count,
    correct_dwelling_count_in_apartment_buildings,
    fill_null_construction_classes,
)
from .creation.boundary import run_creation as run_boundary_creation
from .creation.dwelling import run_creation as run_dwelling_creation
from .inference.dwelling import run_dwelling_inference as run_dwelling_inference
from .inference.building_attributes import (
    run_building_inference as run_building_inference,
)
from .inference.gas_network_connection import (
    run_gas_network_inference as run_gas_network_inference,
)
from .inference.boundary import run_boundary_inference as run_boundary_inference
from .models.climate import run_models as run_climate_models
from .models.solar_masks import run_models as run_solar_mask_models
from .models.solar_gains import run_models as run_solar_gain_models
from .models.thermal_losses import run_models as run_thermal_loss_models
from .models.thermal_needs import run_models as run_thermal_need_models
from .models.energy_consumption import run_models as run_energy_consumption_models
from .models.dwelling_needs import run_models as run_dwelling_need_models
from .models.energy_indicators import run_models as run_energy_indicators
from .exceptions import ModelListError


def select_climate_data(buildings, building_geometries, parameters):
    """

    Args:
        buildings:

    Returns:

    """
    from buildingmodel import data_path

    if parameters.climate_folder is not None:
        climate_path = parameters.climate_folder
    else:
        climate_path = Path(data_path["climate"])
    climate_data_files = {"geometry": [], "file_name": []}

    for i, climate_file in enumerate(climate_path.glob("**/*.epw")):
        with climate_file.open() as f:
            first_line_data = f.readline().split(",")
            climate_geom = Point([float(first_line_data[7]), float(first_line_data[6])])
            climate_data_files["geometry"].append(climate_geom)
            climate_data_files["file_name"].append(climate_file)

    climate_data_files = gpd.GeoDataFrame(climate_data_files, crs="EPSG:4326")
    climate_data_files = climate_data_files.to_crs(building_geometries.crs)
    building_bound_center = Point(
        [building_geometries.centroid.x.mean(), building_geometries.centroid.y.mean()]
    )
    climate_data_files["distances"] = climate_data_files.geometry.distance(
        building_bound_center
    )

    return climate_data_files.sort_values("distances").iloc[0]["file_name"]


ALL_MODELS = [
    "climate",
    "solar_gains",
    "thermal_losses",
    "thermal_needs",
    "dwelling_needs",
    "energy_consumption",
    "energy_indicators",
]


def add_building_result_columns(buildings, parameters):
    """
    Adds building result columns to dataframe
    Args:
        buildings:

    Returns:

    """
    result_columns = []

    # Thermal loss models
    if "thermal_losses" in parameters.models:
        for mode in ["conventional", "actual"]:
            result_columns.append(f"{mode}_heating_set_point")
            result_columns.append(f"{mode}_unified_degree_hours")
            result_columns.append(f"{mode}_ventilation_losses")

        result_columns.append("heating_season_duration")
        result_columns.append("maximal_temperature_difference")
        result_columns.append("peak_ventilation_losses")

    # Thermal need models
    if "thermal_needs" in parameters.models:
        result_columns.append("heated_area_share")
        result_columns.append("conventional_intermittency_factor")
        result_columns.append("regulation_factor")
        result_columns.append("intermittency_factor")

        for mode in ["conventional", "annual", "peak"]:
            result_columns.append(f"{mode}_heating_needs")

        for res in ["solar_gains", "boundary_losses"]:
            result_columns.append(f"annual_{res}")

    # Energy consumption
    if "energy_consumption" in parameters.models:
        for use in ["specific", "cooking", "dhw"]:
            for e_t in ["annual", "peak"]:
                result_columns.append(f"{e_t}_{use}_needs")

        for energy in parameters.energy_list:
            for use in ["consumption", "heating", "dhw"]:
                for e_t in ["annual", "peak"]:
                    result_columns.append(f"{e_t}_{energy}_{use}")

        for e_t in ["annual", "peak"]:
            result_columns.append(f"{e_t}_electricity_specific")

        for energy in ["electricity", "fossil_gas", "liquified_petroleum_gas"]:
            result_columns.append(f"annual_{energy}_cooking")
            result_columns.append(f"peak_{energy}_cooking")

        for energy in parameters.energy_list:
            result_columns.append(f"conventional_{energy}_consumption")
            result_columns.append(f"conventional_{energy}_heating")
            result_columns.append(f"conventional_{energy}_dhw")

        result_columns.append("conventional_dhw_needs")

    if "energy_indicators" in parameters.models:
        result_columns.append("diagnosis_class")

        for mode in ["conventional", "total", "peak"]:
            for e_t in ["final", "primary"]:
                result_columns.append(f"{mode}_{e_t}_consumption")
                result_columns.append(f"{mode}_{e_t}_consumption_by_surface")

    buildings = buildings.drop(
        columns=[c for c in result_columns if c in buildings.columns]
    )
    buildings = pd.concat(
        [
            buildings,
            pd.DataFrame(data=0.0, columns=result_columns, index=buildings.index),
        ],
        axis=1,
    )

    return buildings


def check_model_list(model_list):
    """checks a model list for validity

    A model list is valid if it is a slice of the N first elements of :attr:`buildingmodel.main.ALL_MODELS` with N > 0

    Args:
        model_list (list of str): a list of model names in :attr:`buildingmodel.main.ALL_MODELS`

    Returns:
        bool: True if model list is valid
    """

    model_count = len(model_list)

    if model_count > 0 and (
        (model_list == ALL_MODELS[:model_count])
        | (model_list == ALL_MODELS[model_count:])
    ):
        return True
    else:
        raise ModelListError(model_list, ALL_MODELS)


#
#
# def run_parallel_inference(building_data, climate_data, parameters):
#     """
#     Loads building and climate data and runs the creation of boundaries and dwellings once. Then run multiple
#     inferences and resulting energy models in parallel. The number of iteration is defined in parameters.max_iteration.
#     The number of cpus used is defined in parameters.n_cpu.
#     Args:
#         building_data:
#         climate_data:
#         parameters:
#
#     Returns:
#
#     """
#     p = Pool(processes=parameters.n_cpu)
#     buildings, climate, metadata = load_data(building_data, climate_data, parameters)
#     buildings, boundaries, dwellings = run_creation(buildings, parameters)
#     result_list = []
#     iteration = 0
#     data = p.map(run_inference_and_models, [{'buildings': copy.deepcopy(buildings),
#                                              'boundaries': copy.deepcopy(boundaries),
#                                              'climate': copy.deepcopy(climate),
#                                              'dwellings': copy.deepcopy(dwellings),
#                                              'metadata': copy.deepcopy(metadata),
#                                              'parameters': copy.deepcopy(parameters)} for i in range(parameters.max_iteration)])
#
#     p.close()
#
#     return data, get_case_summary(data)


# def get_case_summary(case_data):
#     """
#
#     Args:
#         case_data:
#
#     Returns:
#
#     """
#
#     building_param_dict = {
#         'annual_specific_needs': 'sum',
#         'annual_dhw_needs': 'sum',
#         'annual_heating_needs': 'sum',
#         'annual_ventilation_losses': 'sum',
#         'annual_solar_gains': 'sum',
#         'annual_occupant_gains': 'sum',
#         'annual_boundary_losses': 'sum',
#         'total_final_consumption': 'sum',
#         'conventional_final_consumption': 'sum',
#         'total_primary_consumption': 'sum',
#         'conventional_primary_consumption': 'sum',
#         'total_final_consumption_by_surface': 'mean',
#         'conventional_final_consumption_by_surface': 'mean',
#         'total_primary_consumption_by_surface': 'mean',
#         'conventional_primary_consumption_by_surface': 'mean',
#         'heating_season_duration': 'first',
#         'actual_unified_degree_hours': 'first',
#         'living_area': 'sum',
#         'floor_area': 'sum',
#         'dwelling_count': 'sum',
#         'actual_heating_set_point': 'mean',
#     }
#     if 'iteration_id' in case_data[0]['buildings'].columns:
#         building_param_dict = {**building_param_dict, 'iteration_id': 'first'}
#
#     cooking_fuel_list = ['electricity', 'liquified_petroleum_gas', 'fossil_gas']
#     fuel_list = cooking_fuel_list + ['oil', 'biomass', 'district_network']
#     use_list = ['consumption', 'heating', 'dhw']
#     columns = [f'annual_{fuel}_{use}' for fuel in fuel_list for use in use_list] +\
#               [f'peak_{fuel}_{use}' for fuel in fuel_list for use in use_list] +\
#               list(building_param_dict.keys()) + ['annual_electricity_specific']
#     columns += [f'annual_{fuel}_cooking' for fuel in cooking_fuel_list]
#     case_summary = pd.DataFrame(index=range(len(case_data)),
#                                 columns=columns,
#                                 dtype='float64')
#
#     for idx, case in enumerate(case_data):
#         buildings = case['buildings']
#         buildings_sim = buildings.loc[buildings.to_sim]
#         for param, agg_m in building_param_dict.items():
#             if agg_m == 'sum':
#                 case_summary.loc[idx, param] = buildings_sim[param].sum()
#             else:
#                 case_summary.loc[idx, param] = buildings_sim[param].values[0]
#
#         for fuel in fuel_list:
#             for use in use_list:
#                 case_summary.loc[idx, f'annual_{fuel}_{use}'] = buildings_sim[f'annual_{fuel}_{use}'].sum()
#                 case_summary.loc[idx, f'peak_{fuel}_{use}'] = buildings_sim[f'peak_{fuel}_{use}'].sum()
#
#         for cooking_fuel in cooking_fuel_list:
#             case_summary.loc[idx, f'annual_{cooking_fuel}_cooking'] = buildings_sim[f'annual_{cooking_fuel}_cooking'].sum()
#             case_summary.loc[idx, f'peak_{cooking_fuel}_cooking'] = buildings_sim[f'peak_{cooking_fuel}_cooking'].sum()
#
#         case_summary.loc[idx, f'annual_electricity_specific'] = buildings_sim[f'annual_electricity_specific'].sum()
#
#     return case_summary


# def run_inference_and_models(input_dict):
#     """
#     run all inferences and all energy models.
#     Args:
#         building_data:
#         climate_data:
#         parameters:
#
#     Returns:
#
#     """
#     input_dict['dwellings'], input_dict['buildings'] = run_building_inference(input_dict['buildings'],
#                                                                      input_dict['dwellings'], input_dict['boundaries'], input_dict['parameters'])
#
#     input_dict['buildings'] = add_building_result_columns(input_dict['buildings'], input_dict['parameters'])
#     run_models(input_dict['climate'], input_dict['metadata'], input_dict['buildings'], input_dict['boundaries'],
#                input_dict['dwellings'], input_dict['parameters'])
#
#     return input_dict


class Parameters(object):
    """
    A class containing all the parameters necessary for a building simulation
    """

    def __init__(
        self,
        simplification_tolerance=5.0,
        models=None,
        grid_resolution=5.0,
        angular_resolution=20.0,
        bbox_filter=100.0,
        albedo=2.0,
        heating_season_start=datetime.datetime(2019, 10, 1),
        heating_season_end=datetime.datetime(2020, 5, 20),
        rename_dict=None,
        usage_dict=None,
        category_filters=None,
        id_col="cleabs",
        minimal_floor_area=50.0,
        minimal_height=2.0,
        vertical_adjacency_tolerance=1.0,
        minimal_boundary_area=2.0,
        encoding="utf8",
        floor_height=None,
        date_format="%Y/%m/%d",
        conventional_heating_set_point=19.0,
        actual_heating_set_point=20.0,
        maximal_occupant_gain_share=0.3,
        maximal_solar_gain_share=0.3,
        temperature_altitude_correction=0.2,
        max_iteration=5,
        construction_year_class=None,
        districts="districts.parquet",
        census="census.parquet",
        diagnosis="energy_performance_diagnosis.parquet",
        gas_network_route="gas_network_route.gpkg",
        minimal_diagnosis_share=0.1,
        gas_network_margin=0.1,
        primary_energies=None,
        energy_list=None,
        n_cpu=8,
        climate_folder=None,
        correct_class_imbalance=True,
        set_point_list=None,
        minimal_apartment_floor_area=100.0,
        census_join_quantile=0.9,
        diagnosis_join_quantile=0.9,
        energy_use_factor=1.0,
        occupation_factors=None,
        parametric_study=None,
    ):
        if models is None:
            models = ALL_MODELS

        if rename_dict is None:
            rename_dict = {
                "usage_1": "main_usage",
                "usage_2": "secondary_usage",
                "date_d_apparition": "construction_date",
                "nombre_de_logements": "dwelling_count",
                "nombre_d_etages": "floor_count",
                "materiaux_des_murs": "wall_material",
                "materiaux_de_la_toiture": "roof_material",
                "hauteur": "height",
                "altitude_minimale_sol": "altitude_min",
                "altitude_maximale_sol": "altitude_max",
            }

        if usage_dict is None:
            usage_dict = {
                "Résidentiel": "residential",
                "Agricole": "agriculture",
                "Annexe": "annex",
                "Commercial et services": "commercial",
                "Indifférencié": "unknown",
                "Industriel": "industrial",
                "Religieux": "religious",
            }

        if category_filters is None:
            category_filters = {"construction_legere": [False]}

        if floor_height is None:
            floor_height = {
                "house": {
                    "min": 2.0,
                    "max": 2.6,
                },
                "apartment": {
                    "min": 2.6,
                    "max": 3.5,
                },
                "other": {
                    "min": 2.6,
                    "max": 3.5,
                },
            }

        if construction_year_class is None:
            construction_year_class = {
                1: [1000, 1918],
                2: [1919, 1945],
                3: [1946, 1970],
                4: [1971, 1990],
                5: [1991, 2005],
                6: [2006, 2012],
                7: [2013, 2100],
            }

        if primary_energies is None:
            primary_energies = {
                "electricity": 2.58,
                "fossil_gas": 1,
                "liquified_petroleum_gas": 1,
                "oil": 1,
                "biomass": 0.6,
                "district_network": 0.6,
            }
        if energy_list is None:
            energy_list = [
                "electricity",
                "fossil_gas",
                "liquified_petroleum_gas",
                "oil",
                "biomass",
                "district_network",
            ]

        if occupation_factors is None:
            occupation_factors = {
                "primary residence": 0.7,
                "occasional housing": 0.2,
                "second home": 0.1,
            }

        self.simplification_tolerance = simplification_tolerance
        self.models = models
        self.grid_resolution = grid_resolution
        self.angular_resolution = angular_resolution
        self.bbox_filter = bbox_filter
        self.albedo = albedo
        self.heating_season_start = heating_season_start
        self.heating_season_end = heating_season_end
        self.rename_dict = rename_dict
        self.usage_dict = usage_dict
        self.category_filters = category_filters
        self.id_col = id_col
        self.minimal_floor_area = minimal_floor_area
        self.minimal_height = minimal_height
        self.floor_height = floor_height
        self.vertical_adjacency_tolerance = vertical_adjacency_tolerance
        self.minimal_boundary_area = minimal_boundary_area
        self.encoding = encoding
        self.date_format = date_format
        self.conventional_heating_set_point = conventional_heating_set_point
        self.actual_heating_set_point = actual_heating_set_point
        self.maximal_occupant_gain_share = maximal_occupant_gain_share
        self.maximal_solar_gain_share = maximal_solar_gain_share
        self.max_iteration = max_iteration
        self.construction_year_class = construction_year_class
        self.districts = districts
        self.gas_network_route = gas_network_route
        self.census = census
        self.diagnosis_file = diagnosis
        self.minimal_diagnosis_share = minimal_diagnosis_share
        self.gas_network_margin = gas_network_margin
        self.primary_energies = primary_energies
        self.energy_list = energy_list
        self.n_cpu = n_cpu
        self.climate_folder = climate_folder
        self.correct_class_imbalance = correct_class_imbalance
        self.temperature_altitude_correction = temperature_altitude_correction
        self.set_point_list = set_point_list
        self.minimal_apartment_floor_area = minimal_apartment_floor_area
        self.census_join_quantile = census_join_quantile
        self.diagnosis_join_quantile = diagnosis_join_quantile
        self.energy_use_factor = energy_use_factor
        self.occupation_factors = occupation_factors
        self.parametric_study = parametric_study


class Simulation(object):
    """ """

    def __init__(self, building_data, climate_data, parameters):
        self.buildings = building_data
        self.climate_data = climate_data
        self.parameters = parameters
        self.building_geometries = None
        self.climate = None
        self.climate_metadata = None
        self.census = None
        self.diagnosis = None
        self.boundaries = None
        self.dwellings = None

    def clone(self):
        new_simulation = Simulation(
            self.buildings.clone(), self.climate_data, self.parameters
        )
        new_simulation.building_geometries = self.building_geometries.copy()
        new_simulation.climate = self.climate.copy()
        new_simulation.climate_metadata = self.climate_metadata.copy()
        new_simulation.census = self.census
        new_simulation.diagnosis = self.diagnosis
        new_simulation.boundaries = self.boundaries.clone()
        new_simulation.dwellings = self.dwellings.clone()
        return new_simulation

    def load_data(self):
        """
        Loads building and climate data. If climate data is not specified, the climate data file with the closest position
        is selected in data/weather

        Args:

        Returns:

        """
        self.buildings, self.building_geometries = load_gis_data(
            self.buildings, self.parameters
        )
        if self.climate_data is None:
            self.climate_data = select_climate_data(
                self.buildings, self.building_geometries, self.parameters
            )
        self.climate, self.climate_metadata = load_climate_data(self.climate_data)
        self.climate_metadata["building_altitude"] = self.buildings["altitude"].mean()

        department_list = list(self.buildings["department"].unique())
        self.census = load_census_in_case(department_list, self.parameters)
        self.diagnosis = load_diagnosis_data(self.parameters)

        if self.parameters.correct_class_imbalance:
            self.buildings = correct_dwelling_count_in_apartment_buildings(
                self.buildings, self.parameters
            )
            self.buildings = correct_dwelling_count(
                self.buildings, self.parameters, self.census
            )
            self.buildings = fill_null_construction_classes(self.buildings, self.census)

        self.buildings = self.buildings.with_columns(
            to_sim=pl.col("dwelling_count") >= 1
        )

        if self.buildings.filter(pl.col("to_sim")).shape[0] == 0:
            raise ValueError(
                "Simulation cannot run as there are no residential buildings."
            )

    def run_creation(self):
        """
        Creates boundaries and dwellings from building geometry and parameters

        Args:
            buildings (GeoDataframe): a GeoDataframe containing the building geometries and parameters
            parameters (Parameters): the global parameter variable. Here it includes e.g. tolerance applied as pas of the simplification algorithm (in m²). Defaults to 2.

        Returns:

        """

        # Boundary
        self.boundaries, self.buildings, self.building_geometries = (
            run_boundary_creation(
                self.buildings, self.building_geometries, self.parameters
            )
        )
        self.dwellings = run_dwelling_creation(self.buildings)
        self.boundaries = run_solar_mask_models(
            self.building_geometries, self.boundaries, self.parameters
        )

    def run_inference(self):
        """
        Inference of dwelling and boundary parameters for energy modelling

        Args:
            buildings:
            dwellings:
            boundaries:

        Returns:

        """
        self.buildings = run_gas_network_inference(
            self.buildings, self.building_geometries, self.parameters, self.census
        )
        self.dwellings, self.buildings = run_dwelling_inference(
            self.dwellings, self.buildings, self.parameters, self.census
        )
        self.buildings = run_building_inference(
            self.buildings, self.dwellings, self.parameters
        )
        self.boundaries = run_boundary_inference(
            self.buildings, self.boundaries, self.parameters
        )

    def run_models(self):
        """buildingmodel main function

        Loads building and weather data and runs the models in model_list in sequential order

        Args:

        Returns:
            tuple of DataFrame and GeoDataFrame, containing the results of the models
        """

        # check_model_list(parameters.models)

        # Climate models
        if "climate" in self.parameters.models:
            run_climate_models(self.climate, self.climate_metadata, self.parameters)

        # Solar gain models
        if "solar_gains" in self.parameters.models:
            self.boundaries = run_solar_gain_models(
                self.boundaries, self.climate, self.parameters
            )

        # Thermal loss models
        if "thermal_losses" in self.parameters.models:
            self.buildings, self.boundaries = run_thermal_loss_models(
                self.buildings, self.boundaries, self.climate, self.parameters
            )

        # dwelling needs
        if "dwelling_needs" in self.parameters.models:
            self.dwellings = run_dwelling_need_models(
                self.dwellings, self.climate, self.parameters
            )

        # Thermal need models
        if "thermal_needs" in self.parameters.models:
            self.buildings = run_thermal_need_models(
                self.buildings, self.boundaries, self.dwellings, self.parameters
            )

        # Energy consumption
        if "energy_consumption" in self.parameters.models:
            self.buildings = run_energy_consumption_models(
                self.buildings, self.dwellings, self.parameters
            )

        if "energy_indicators" in self.parameters.models:
            self.buildings = run_energy_indicators(self.buildings, self.parameters)

    def run_all(self):
        """
        Run complete building simulation :
            * Load building and climate data
            * Creation of boundaries and dwellings
            * Inference of boundary, dwelling and heating system parameters
            * Run energy models

        Args:
            building_data:
            climate_data:
            parameters: instance of class Parameters

        Returns:

        """

        self.load_data()
        self.run_creation()
        self.run_inference()
        self.run_models()

    def run_multiple_inferences(self):
        self.load_data()
        self.run_creation()
        res_to_clear = ["buildings", "boundaries", "dwellings"]
        c_cols = {k: getattr(self, k).columns for k in res_to_clear}

        result_list = []
        iteration = 0

        while True:
            for r in res_to_clear:
                setattr(self, r, getattr(self, r).select(c_cols[r]))

            self.run_inference()

            if self.parameters.parametric_study is not None:
                solar_models = ["climate", "solar_gains"]
                other_models = [
                    model for model in ALL_MODELS if model not in solar_models
                ]
                self.parameters.models = solar_models
                self.run_models()
                i_cols = {k: getattr(self, k).columns for k in res_to_clear}
                self.parameters.models = other_models
                for i, params in enumerate(self.parameters.parametric_study):
                    for r in res_to_clear:
                        setattr(self, r, getattr(self, r).select(i_cols[r]))

                    info_dict = {"iteration_id": pl.lit(iteration)}
                    for name, value in params.items():
                        setattr(self.parameters, name, value)
                        info_dict[name] = pl.lit(value)

                    self.run_models()

                    res_dict = {
                        name: getattr(self, name).with_columns(**info_dict)
                        for name in ["buildings", "boundaries", "dwellings"]
                    }

                    result_list.append(res_dict)
            else:
                self.run_models()

                res_dict = {
                    name: getattr(self, name).with_columns(
                        iteration_id=pl.lit(iteration)
                    )
                    for name in ["buildings", "boundaries", "dwellings"]
                }

                result_list.append(res_dict)

            iteration += 1
            if iteration >= self.parameters.max_iteration:
                return result_list


# def get_case_summary(case_data):
#     """
#
#     Args:
#         case_data:
#
#     Returns:
#
#     """
#
#     building_param_dict = {
#         'annual_specific_needs': 'sum',
#         'annual_dhw_needs': 'sum',
#         'annual_heating_needs': 'sum',
#         'annual_ventilation_losses': 'sum',
#         'annual_solar_gains': 'sum',
#         'annual_occupant_gains': 'sum',
#         'annual_boundary_losses': 'sum',
#         'total_final_consumption': 'sum',
#         'conventional_final_consumption': 'sum',
#         'total_primary_consumption': 'sum',
#         'conventional_primary_consumption': 'sum',
#         'total_final_consumption_by_surface': 'mean',
#         'conventional_final_consumption_by_surface': 'mean',
#         'total_primary_consumption_by_surface': 'mean',
#         'conventional_primary_consumption_by_surface': 'mean',
#         'heating_season_duration': 'first',
#         'actual_unified_degree_hours': 'first',
#         'living_area': 'sum',
#         'floor_area': 'sum',
#         'dwelling_count': 'sum',
#         'actual_heating_set_point': 'mean',
#     }
#     if 'iteration_id' in case_data[0]['buildings'].columns:
#         building_param_dict = {**building_param_dict, 'iteration_id': 'first'}
#
#     cooking_fuel_list = ['electricity', 'liquified_petroleum_gas', 'fossil_gas']
#     fuel_list = cooking_fuel_list + ['oil', 'biomass', 'district_network']
#     use_list = ['consumption', 'heating', 'dhw']
#     columns = [f'annual_{fuel}_{use}' for fuel in fuel_list for use in use_list] +\
#               [f'peak_{fuel}_{use}' for fuel in fuel_list for use in use_list] +\
#               list(building_param_dict.keys()) + ['annual_electricity_specific']
#     columns += [f'annual_{fuel}_cooking' for fuel in cooking_fuel_list]
#     case_summary = pd.DataFrame(index=range(len(case_data)),
#                                 columns=columns,
#                                 dtype='float64')
#
#     for idx, case in enumerate(case_data):
#         buildings = case['buildings']
#         buildings_sim = buildings.loc[buildings.to_sim]
#         for param, agg_m in building_param_dict.items():
#             if agg_m == 'sum':
#                 case_summary.loc[idx, param] = buildings_sim[param].sum()
#             else:
#                 case_summary.loc[idx, param] = buildings_sim[param].values[0]
#
#         for fuel in fuel_list:
#             for use in use_list:
#                 case_summary.loc[idx, f'annual_{fuel}_{use}'] = buildings_sim[f'annual_{fuel}_{use}'].sum()
#                 case_summary.loc[idx, f'peak_{fuel}_{use}'] = buildings_sim[f'peak_{fuel}_{use}'].sum()
#
#         for cooking_fuel in cooking_fuel_list:
#             case_summary.loc[idx, f'annual_{cooking_fuel}_cooking'] = buildings_sim[f'annual_{cooking_fuel}_cooking'].sum()
#             case_summary.loc[idx, f'peak_{cooking_fuel}_cooking'] = buildings_sim[f'peak_{cooking_fuel}_cooking'].sum()
#
#         case_summary.loc[idx, f'annual_electricity_specific'] = buildings_sim[f'annual_electricity_specific'].sum()
#
#     return case_summary
