# -*- coding: utf-8 -*-

import geopandas as gpd
import numpy as np
import polars as pl
import topojson
from shapely.geometry import LineString
from buildingmodel.logger import duration_logging

# Integer variables for boundary types
EXTERIOR_WALL = 0
INTERIOR_WALL = 1
ROOF = 2
FLOOR = 3


@duration_logging
def simplify_geometry(buildings, building_geometries, parameters):
    """
    Creates a topojson object from the building footprints and applies quantization and simplification while preserving
    topology

    Args:
        buildings (GeoDataframe): a GeoDataframe containing the building geometries and parameters
        parameters

    Returns:
        a topojson.Topology object
    """
    from buildingmodel.io.gis import clean_geometry

    tj = topojson.Topology(
        building_geometries,
        prequantize=False,
        topology=True,
        simplify_with="simplification",
        simplify_algorithm="vw",
        toposimplify=parameters.simplification_tolerance,
        winding_order="CCW_CW",
    )
    building_geometries = tj.to_gdf(crs="EPSG:2154")
    building_geometries = clean_geometry(building_geometries)
    buildings = buildings.filter(
        pl.col("building_id").is_in(building_geometries["building_id"].tolist())
    )

    return tj, buildings, building_geometries


@duration_logging
def detect_adjacency(tj, building_ids):
    """
    loop through the arcs created by topojson to find the buildings for which each arc is a part of the footprint

    Args:
        tj: a topojson.Topology object created from the building geometries
        building_ids: a list of building IDs

    Returns:
        a dict with keys the index of the arcs and values a dict containing the arcs' coordinates and the indices
    of the buildings for which the arc is a part of the footprint

    """

    arcs = tj.output["arcs"]
    buildings = tj.output["objects"]["data"]["geometries"]

    arc_dict = {
        i: {"coordinates": None, "building_indices": []} for i in range(len(arcs))
    }
    # arc_dict['transform'] = tj.output['transform']

    for i, arc in enumerate(arcs):
        arc_dict[i]["coordinates"] = np.array(arc)
        for k, building in enumerate(buildings):
            for build_arc in building["arcs"]:
                if (i in build_arc) or (-(i + 1) in build_arc):
                    arc_dict[i]["building_indices"].append(
                        building["properties"]["building_id"]
                    )

    arc_dict = {
        k: v
        for k, v in arc_dict.items()
        if all([b_id in building_ids for b_id in v["building_indices"]])
    }

    return arc_dict


@duration_logging
def create_boundaries(arc_dict, buildings, building_geometries, parameters):
    """
    Create the boundaries of a building (exterior and interior walls, exterior roofs and floors

    Args:
        arc_dict: a dict with keys the index of the arcs and values a dict containing the arcs' coordinates and the indices
    of the buildings for which the arc is a part of the footprint
        buildings: a GeoDataframe containing the building geometries and parameters

    Returns:
        A GeoDataframe containing the boundary geometry and attributes (type, building_id, area, center, orientation)
    """

    boundary_dict = {
        "height": [],
        "altitude": [],
        "center_x": [],
        "center_y": [],
        "center_z": [],
        "area": [],
        "type": [],
        "building_id": [],
        "azimuth": [],
        "floor_count": [],
        "adjacency_usage": [],
    }

    # transform = arc_dict.pop('transform')
    transform = None

    for arc in arc_dict.values():
        coords = arc["coordinates"]
        if len(arc["building_indices"]) == 1:
            # arc belongs to only one building so we create the walls directly
            building = buildings.filter(
                pl.col("building_id") == arc["building_indices"][0]
            )
            create_vertical_boundary(
                coords,
                building["height"][0],
                building["altitude"][0],
                building["floor_count"][0],
                building["building_id"][0],
                None,
                EXTERIOR_WALL,
                transform,
                boundary_dict,
            )
        elif len(arc["building_indices"]) == 2:
            # arc belongs to two buildings so we create the walls depending on the relative height and altitude of
            # the buildings
            building_0 = buildings.filter(
                pl.col("building_id") == arc["building_indices"][0]
            )
            building_1 = buildings.filter(
                pl.col("building_id") == arc["building_indices"][1]
            )
            create_vertical_adjacent_boundaries(
                coords, building_0, building_1, boundary_dict, transform, parameters
            )

    for building_row in buildings.iter_rows(named=True):
        geom = building_geometries.loc[
            building_geometries["building_id"] == building_row["building_id"],
            "geometry",
        ].iloc[0]
        for is_roof in [True, False]:
            create_horizontal_boundary(
                geom,
                building_row["height"],
                building_row["altitude"],
                boundary_dict,
                is_roof,
                building_row["building_id"],
            )

    return pl.DataFrame(boundary_dict)


def create_vertical_boundary(
    coords,
    height,
    altitude,
    floor_count,
    building_id,
    adjacency_usage,
    type,
    transform,
    boundary_dict,
):
    """
    Calculates the geometrical characteristics of a vertical boundary from a topojson arc and the height and altitude
    of the boundary
    Args:
        coords:
        height:
        altitude:
        floor_count:
        building_id:
        adjacency_usage:
        type:
        transform:
        boundary_dict:

    Returns:

    """

    if height == 0:
        return None

    for i in range(coords.shape[0] - 1):
        # abs_coords = list(rel2abs(coords[i:(i + 2), :], transform['scale'], transform['translate']))
        abs_coords = coords[i : (i + 2), :]
        boundary_dict["height"].append(height)
        boundary_dict["altitude"].append(altitude)
        boundary_dict["floor_count"].append(floor_count)
        boundary_dict["center_x"].append((abs_coords[0][0] + abs_coords[1][0]) / 2.0)
        boundary_dict["center_y"].append((abs_coords[0][1] + abs_coords[1][1]) / 2.0)
        boundary_dict["center_z"].append(altitude + height / 2.0)
        boundary_dict["type"].append(type)
        boundary_dict["azimuth"].append(azimuth(abs_coords))
        length = np.sqrt(
            (abs_coords[1][0] - abs_coords[0][0]) ** 2
            + (abs_coords[1][1] - abs_coords[0][1]) ** 2
        )
        boundary_dict["area"].append(length * height)
        boundary_dict["building_id"].append(building_id)
        boundary_dict["adjacency_usage"].append(adjacency_usage)


def create_horizontal_boundary(
    footprint, height, altitude, boundary_dict, is_roof, building_index
):
    centroid = footprint.centroid
    if is_roof:
        z = height + altitude
        boundary_type = 2
    else:
        z = altitude
        boundary_type = 3

    boundary_dict["height"].append(0.0)
    boundary_dict["altitude"].append(z)
    boundary_dict["floor_count"].append(0)
    boundary_dict["center_x"].append(centroid.x)
    boundary_dict["center_y"].append(centroid.y)
    boundary_dict["center_z"].append(z)
    boundary_dict["type"].append(boundary_type)
    boundary_dict["azimuth"].append(0.0)
    boundary_dict["area"].append(footprint.area)
    boundary_dict["building_id"].append(building_index)
    boundary_dict["adjacency_usage"].append(None)


def create_vertical_adjacent_boundaries(
    coords, building_0, building_1, boundary_dict, transform, parameters
):
    """
    Creates the vertical boundaries when two buildings are adjacent, depending on their relative altitude and heights.


    Args:
        coords:
        building_0:
        building_1:
        boundary_dict:
        transform:

    Returns:

    """

    height_0 = building_0["height"][0]
    altitude_0 = building_0["altitude"][0]
    floor_count_0 = building_0["floor_count"][0]
    height_1 = building_1["height"][0]
    altitude_1 = building_1["altitude"][0]
    floor_count_1 = building_1["floor_count"][0]
    id_0 = building_0["building_id"][0]
    id_1 = building_1["building_id"][0]
    usage_0 = building_0["main_usage"][0]
    usage_1 = building_1["main_usage"][0]
    tolerance = parameters.vertical_adjacency_tolerance

    if (
        altitude_1 >= altitude_0 + height_0 - tolerance
        or altitude_0 > altitude_1 + height_1 - tolerance
    ):  # buildings do not overlap
        create_vertical_boundary(
            coords,
            height_0,
            altitude_0,
            floor_count_0,
            id_0,
            None,
            EXTERIOR_WALL,
            transform,
            boundary_dict,
        )
        create_vertical_boundary(
            coords,
            height_1,
            altitude_1,
            floor_count_1,
            id_1,
            None,
            EXTERIOR_WALL,
            transform,
            boundary_dict,
        )

    elif (
        altitude_1 + height_1 >= altitude_0 + height_0 + tolerance
        and altitude_1 <= altitude_0 - tolerance
    ):  # building_1 wall 'contains' building_0 wall
        create_vertical_adjacency_contain(
            coords,
            height_0,
            height_1,
            altitude_0,
            altitude_1,
            floor_count_0,
            floor_count_1,
            id_0,
            id_1,
            usage_0,
            usage_1,
            boundary_dict,
            transform,
        )

    elif (
        altitude_1 + height_1 <= altitude_0 + height_0 - tolerance
        and altitude_1 >= altitude_0 + tolerance
    ):  # building_0 wall 'contains' building_1 wall
        create_vertical_adjacency_contain(
            coords,
            height_1,
            height_0,
            altitude_1,
            altitude_0,
            floor_count_1,
            floor_count_0,
            id_1,
            id_0,
            usage_1,
            usage_0,
            boundary_dict,
            transform,
        )

    elif (
        altitude_1 >= altitude_0
        and altitude_1 + height_1 > altitude_0 + height_0 + tolerance
    ):  # buildings overlap with building_1 higher than building_0
        create_vertical_adjacency_overlap(
            coords,
            height_0,
            height_1,
            altitude_0,
            altitude_1,
            floor_count_0,
            floor_count_1,
            id_0,
            id_1,
            usage_0,
            usage_1,
            boundary_dict,
            transform,
            parameters,
        )

    elif (
        altitude_1 <= altitude_0
        and altitude_1 + height_1 < altitude_0 + height_0 - tolerance
    ):  # buildings overlap with building_0 higher than building_1
        create_vertical_adjacency_overlap(
            coords,
            height_1,
            height_0,
            altitude_1,
            altitude_0,
            floor_count_1,
            floor_count_0,
            id_1,
            id_0,
            usage_1,
            usage_0,
            boundary_dict,
            transform,
            parameters,
        )

    else:  # buildings have the same height and altitude within the tolerance
        create_vertical_boundary(
            coords,
            height_0,
            altitude_0,
            floor_count_0,
            id_0,
            usage_1,
            INTERIOR_WALL,
            transform,
            boundary_dict,
        )
        create_vertical_boundary(
            coords,
            height_1,
            altitude_1,
            floor_count_1,
            id_1,
            usage_0,
            INTERIOR_WALL,
            transform,
            boundary_dict,
        )


def create_vertical_adjacency_contain(
    coords,
    height_0,
    height_1,
    altitude_0,
    altitude_1,
    floor_count_0,
    floor_count_1,
    id_0,
    id_1,
    usage_0,
    usage_1,
    boundary_dict,
    transform,
):
    """
    Create the vertical walls when building_1 contains the adjacency of building 0. Will create two exterior walls for
    building_1, one interior wall for building_1 and one interior wall for building_0

    Args:
        coords:
        height_0:
        height_1:
        altitude_0:
        altitude_1:
        id_0:
        id_1:
        boundary_dict:
        transform:

    Returns:

    """

    create_vertical_boundary(
        coords,
        altitude_1 + height_1 - (altitude_0 + height_0),
        altitude_0 + height_0,
        floor_count_1 / 2.0,
        id_1,
        None,
        EXTERIOR_WALL,
        transform,
        boundary_dict,
    )
    create_vertical_boundary(
        coords,
        altitude_0 - altitude_1,
        altitude_1,
        floor_count_1 / 2.0,
        id_1,
        None,
        EXTERIOR_WALL,
        transform,
        boundary_dict,
    )
    create_vertical_boundary(
        coords,
        height_0,
        altitude_0,
        0.0,
        id_1,
        usage_0,
        INTERIOR_WALL,
        transform,
        boundary_dict,
    )
    create_vertical_boundary(
        coords,
        height_0,
        altitude_0,
        0.0,
        id_0,
        usage_1,
        INTERIOR_WALL,
        transform,
        boundary_dict,
    )


def create_vertical_adjacency_overlap(
    coords,
    height_0,
    height_1,
    altitude_0,
    altitude_1,
    floor_count_0,
    floor_count_1,
    id_0,
    id_1,
    usage_0,
    usage_1,
    boundary_dict,
    transform,
    parameters,
):
    """
    Create the vertical walls when buildings overlap with building_1 higher than building_0. Will create one exterior
    and one interior wall for each building

    Args:
        coords:
        height_0:
        height_1:
        altitude_0:
        altitude_1:
        id_0:
        id_1:
        boundary_dict:
        transform:

    Returns:

    """

    create_vertical_boundary(
        coords,
        height_0 + altitude_0 - altitude_1,
        altitude_1,
        0.0,
        id_1,
        usage_0,
        INTERIOR_WALL,
        transform,
        boundary_dict,
    )
    create_vertical_boundary(
        coords,
        height_0 + altitude_0 - altitude_1,
        altitude_1,
        0.0,
        id_0,
        usage_1,
        INTERIOR_WALL,
        transform,
        boundary_dict,
    )
    create_vertical_boundary(
        coords,
        height_1 + altitude_1 - altitude_0 - height_0,
        altitude_0 + height_0,
        floor_count_1,
        id_1,
        None,
        EXTERIOR_WALL,
        transform,
        boundary_dict,
    )
    if altitude_1 - altitude_0 > parameters.vertical_adjacency_tolerance:
        create_vertical_boundary(
            coords,
            altitude_1 - altitude_0,
            altitude_0,
            floor_count_0,
            id_0,
            None,
            EXTERIOR_WALL,
            transform,
            boundary_dict,
        )


def azimuth(coords):
    """
    Calculates the azimuth of a line in degrees (South is 0)

    :param line: a shapely linestring
    :return:
    """

    azimuth = (
        np.degrees(
            np.arctan2(
                coords[1][0] - coords[0][0],
                coords[1][1] - coords[0][1],
            )
        )
        + 90.0
    )
    if azimuth < 0.0:
        azimuth += 360.0

    return azimuth


def building_level_surface(buildings, boundaries):
    """

    Args:
        buildings:
        boundaries:

    Returns:

    """
    name_dict = {
        "exterior_wall": 0,
        "adjacent_wall": 1,
        "roof": 2,
        "exterior_floor": 3,
    }

    for name, id in name_dict.items():
        areas = (
            boundaries.filter(pl.col("type") == id)
            .select(["building_id", "area"])
            .group_by(["building_id"])
            .sum()
            .rename({"area": f"{name}_area"})
        )
        if areas.shape[0] == 0:
            buildings.with_columns(**{f"{name}_area": pl.lit(0.0)})
        else:
            buildings = buildings.join(areas, on="building_id", how="left")

    return buildings


def run_creation(buildings, building_geometries, parameters):
    """
    runs the function necessary to create the boundaries from the building geometry :
        * geometry simplification
        * adjacency detection
        * creation of boundary table

    Args:
        buildings (GeoDataframe): a GeoDataframe containing the building geometries and parameters
        parameters

    Returns:
        a tuple of GeoDataframes containing the building geometries and parameters and the boundary geometries and parameters
    """
    tj, buildings, building_geometries = simplify_geometry(
        buildings, building_geometries, parameters
    )
    arc_dict = detect_adjacency(tj, buildings["building_id"].to_list())
    boundaries = create_boundaries(arc_dict, buildings, building_geometries, parameters)

    ids_to_sim = buildings.filter(pl.col("to_sim") == True)["building_id"].to_list()
    boundaries = boundaries.filter(
        pl.col("building_id").is_in(ids_to_sim)
        & (pl.col("area") > parameters.minimal_boundary_area)
    )
    buildings = building_level_surface(buildings, boundaries)

    return boundaries, buildings, building_geometries
