import polars as pl

geo_levels = ["district", "city", "city_group", "department", "region"]


def run_creation(buildings):
    """
    Creates the table of dwellings corresponding to the residential buildings using the dwelling_count attribute of
    buildings

    Args:
        buildings (GeoDataframe): a GeoDataframe containing the building geometries and parameters

    Returns:
        dwellings: a Dataframe containing dwellings parameters and results

    Returns:

    """
    buildings = buildings.with_columns(
        floor_area_share=pl.col("floor_area") / pl.col("dwelling_count")
    )

    dwellings = (
        buildings.filter(pl.col("dwelling_count") >= 1.0)
        .with_columns(pl.col("dwelling_count").repeat_by("dwelling_count"))
        .explode("dwelling_count")
        .select(
            [
                "construction_year_class",
                "residential_type",
                "building_id",
                "floor_area_share",
                "residential_only",
                "has_annex",
                "multi_dwelling",
            ]
            + geo_levels
        )
    )
    return dwellings
