import functools
import logging
import time
import polars as pl
from datetime import timedelta
from buildingmodel import log_dir


def create_logger(file_name="buildingmodel", remove_file=True):
    """
    Creates a logging object and returns it
    """
    logger = logging.getLogger("buildingmodel")
    logger.setLevel(logging.DEBUG)
    # Create handlers
    c_handler = logging.StreamHandler()
    log_dir.mkdir(parents=True, exist_ok=True)
    log_path = log_dir / f"{file_name}.log"
    if log_path.exists() and remove_file:
        log_path.unlink()
    f_handler = logging.FileHandler(log_path)
    c_handler.setLevel(logging.INFO)
    f_handler.setLevel(logging.INFO)

    # Create formatters and add it to handlers

    c_formatter = DataFrameFormatter(
        "%(asctime)s %(levelname)-8s %(message)s", n_rows=20
    )
    f_formatter = DataFrameFormatter(
        "%(asctime)s %(levelname)-8s %(message)s", n_rows=20
    )

    c_handler.setFormatter(c_formatter)
    f_handler.setFormatter(f_formatter)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    logger.addHandler(f_handler)
    return logger


def duration_logging(function):
    """
    A decorator that wraps the passed in function and logs the execution time
    """

    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        try:
            start = time.time()
            result = function(*args, **kwargs)
            duration = time.time() - start
            logger = logging.getLogger("buildingmodel")
            logger.info(
                f"duration of {function.__name__} : {timedelta(seconds=duration)}"
            )
            return result
        except:
            # log the exception
            err = "There was an exception in  "
            err += function.__name__
            logger = logging.getLogger("buildingmodel")
            logger.exception(err)
            # re-raise the exception
            raise

    return wrapper


class DataFrameFormatter(logging.Formatter):
    def __init__(self, fmt: str, n_rows: int = 20) -> None:
        self.n_rows = n_rows
        super().__init__(fmt)

    def format(self, record: logging.LogRecord) -> str:
        if isinstance(record.msg, pl.DataFrame):
            n_rows = getattr(record, "n_rows", self.n_rows)
            header = getattr(record, "header", "").strip()

            # Format the DataFrame
            df_str = record.msg.head(n=n_rows).__str__()
            lines = df_str.splitlines()
            lines = [
                l
                for l in lines
                # if ("f64" not in l)
                # and ("str" not in l)
                # and ("cat" not in l)
                # and ("-" not in l)
            ][1:]
            # Preserve original message
            original_msg = record.msg

            formatted_lines = []
            if header:
                formatted_lines.append(
                    super().format(
                        logging.LogRecord(
                            name=record.name,
                            level=record.levelno,
                            pathname=record.pathname,
                            lineno=record.lineno,
                            msg=header,
                            args=(),
                            exc_info=None,
                        )
                    )
                )

            for line in lines:
                formatted_lines.append(
                    super().format(
                        logging.LogRecord(
                            name=record.name,
                            level=record.levelno,
                            pathname=record.pathname,
                            lineno=record.lineno,
                            msg=line,
                            args=(),
                            exc_info=None,
                        )
                    )
                )

            # Restore the original message
            record.msg = original_msg
            return "\n".join(formatted_lines)

        return super().format(record)
