# -*- coding: utf-8 -*-

from pathlib import Path

from environs import Env
from importlib.resources import files


# paths for the various data folders
data_path = {
    "gis": files("buildingmodel") / "data" / "gis",
    "climate": files("buildingmodel") / "data" / "climate",
    "census": files("buildingmodel") / "data" / "census",
    "diagnosis": files("buildingmodel") / "data" / "diagnosis",
    "building_statistics": files("buildingmodel") / "data" / "building_statistics",
    "energy_use": files("buildingmodel") / "data" / "energy_use",
}


# environ file variable
# Load environment file
BASE_DIR = Path(__file__).parent.expanduser().resolve()
log_dir = BASE_DIR / "logs"
environ_path = BASE_DIR / ".env"
env = Env()
Env.read_env(environ_path)

# display plots in tests
plot = env.bool("plot", False)
