import gc
from datetime import datetime
import numpy as np
import pandas as pd
import polars as pl
import pvlib
import numexpr as ne

import buildingmodel.cython_utils.shading as shading_utils
from buildingmodel.logger import duration_logging
from buildingmodel.utils import heating_season, add_parameter_from_building
from ..creation.boundary import EXTERIOR_WALL, INTERIOR_WALL, ROOF, FLOOR


@duration_logging
def mask_influence(solar_exposed_boundaries, climate):
    """This function calculates the influence of the solar masks of each boundary on the diffuse and direct normal
    radiation

    For the direct radiation, at each time step and for each boundary, the sun height is compared to the
    height of the solar mask for the current sun azimuth. If it is higher, the original direct radiation value
    is retained. If not, it is set to 0.

    For the diffuse radiation, a sky view factor for each boundary is calculated and then applied to the diffuse
    radiation (Middel, A., Lukasczyk, J., Maciejewski, R., Demuzere, M., & Roth, M. (2018).
    Sky View Factor footprints for urban climate modeling. Urban climate, 25, 120-134.)

    Args:
        solar_exposed_boundaries (GeoDataframe): a GeoDataframe containing the boundary geometries
        climate (Dataframe): a Dataframe containing climate data

    Returns:
        a tuple of numpy arrays containing the horizontal diffuse and normal direct radiation for each boundary
        and each time step
    """

    mask_columns = [
        col_name
        for col_name in solar_exposed_boundaries.columns
        if col_name.startswith("mask_")
    ]

    masks = solar_exposed_boundaries.select(mask_columns).to_numpy()
    visible_beam = shading_utils.visible_beam(
        masks.astype(np.float32),
        climate["sun_height"].values.astype(np.float32),
        climate["sun_azimuth"].values.astype(np.float32),
    )
    direct_radiation = climate["direct_normal_radiation"].values * visible_beam

    return direct_radiation


def aoi(surface_tilt, surface_azimuth, solar_zenith, solar_azimuth):
    surface_tilt = np.radians(surface_tilt)
    surface_azimuth = np.radians(surface_azimuth)
    solar_zenith = np.radians(solar_zenith)
    solar_azimuth = np.radians(solar_azimuth)

    projection = ne.evaluate(
        "cos(surface_tilt) * cos(solar_zenith) + sin(surface_tilt) * sin(solar_zenith) * cos(solar_azimuth - surface_azimuth)"
    )
    projection = np.clip(projection, -1, 1)
    aoi_value = ne.evaluate("arccos(projection)")

    return aoi_value


@duration_logging
def radiation_on_boundary_model(solar_exposed_boundaries, direct_radiation, climate):
    """Calculation of the angle of incidence and diffuse and direct radiation on each boundary

    Returns:

    """

    boundary_inclinations = get_boundary_inclination(solar_exposed_boundaries)
    boundary_count = solar_exposed_boundaries.shape[0]

    sun_azimuth = np.tile(climate["sun_azimuth"].values, [boundary_count, 1])
    zenith = 90.0 - climate["sun_height"].values
    angle_of_incidence = aoi(
        boundary_inclinations.reshape((-1, 1)),
        solar_exposed_boundaries["azimuth"].to_numpy().reshape((-1, 1)),
        zenith,
        sun_azimuth,
    )

    poa_direct = np.maximum(
        ne.evaluate("direct_radiation * cos(angle_of_incidence)"), 0.0
    )
    return poa_direct, angle_of_incidence


def get_boundary_inclination(solar_exposed_boundaries):
    """

    Args:
        solar_exposed_boundaries:

    Returns:

    """

    boundary_inclinations = np.zeros(solar_exposed_boundaries.shape[0])
    boundary_inclinations[solar_exposed_boundaries["type"] == EXTERIOR_WALL] = 90.0

    return boundary_inclinations


@duration_logging
def transmission_model(
    solar_exposed_boundaries, direct_radiation, angle_of_incidence, heating_period_mask
):
    """Calculates the total solar gains absorbed by the boundaries (opaque and windows) and transmitted (windows)
    during the heating season when the air temperature is below the heating set point

    Args:
        solar_exposed_boundaries (GeoDataframe): a GeoDataframe containing the boundary geometries
        direct_radiation:
        diffuse_radiation:
        angle_of_incidence:
        heating_period_mask: a numpy array of mask

    Returns:
        GeoDataframe with columns absorbed_solar_gain and transmitted_solar_gain in kWh
    """

    tiled_mask = np.tile(heating_period_mask, [solar_exposed_boundaries.shape[0], 1])

    direct_radiation = ne.evaluate("direct_radiation * tiled_mask")

    solar_exposed_boundaries = solar_exposed_boundaries.with_columns(
        window_area=pl.col("window_share") * pl.col("area"),
        opaque_area=(1.0 - pl.col("window_share")) * pl.col("area"),
    )

    window_transmission_factor = (
        solar_exposed_boundaries["window_solar_factor"].to_numpy().reshape((-1, 1))
    )
    transmission_coefficient = np.clip(
        ne.evaluate(
            "(1.0 - (angle_of_incidence / 90.0) ** 5) * window_transmission_factor"
        ),
        0.0,
        1.0,
    )
    direct_transmission = ne.evaluate(
        "sum(transmission_coefficient * direct_radiation / 1000., axis=1)"
    )

    solar_exposed_boundaries = solar_exposed_boundaries.with_columns(
        transmitted_solar_gain=pl.col("window_area") * pl.Series(direct_transmission)
    )

    return solar_exposed_boundaries


@duration_logging
def heating_periods(climate, parameters):
    """Calculates the time steps belonging to heating season and for which the air temperature is below the heating set
    point.

    Returns:
        pandas.DataFrame
    """
    heating_season_index = heating_season(
        climate, parameters.heating_season_start, parameters.heating_season_end
    )
    heating_season_mask = climate.index.isin(heating_season_index)
    set_point = parameters.actual_heating_set_point

    mask_array = np.zeros(len(climate.index))
    mask_array[climate["air_temperature"] < set_point] = 1.0
    mask_array[~heating_season_mask] = 0.0

    return mask_array


def run_models(boundaries, climate, parameters):
    """

    Args:
        boundaries (GeoDataframe): a GeoDataframe containing the boundary geometries
        climate (Dataframe): a Dataframe containing climate data
        parameters

    Returns:

    """
    c_hs = parameters.conventional_heating_set_point
    a_hs = parameters.actual_heating_set_point
    boundaries = boundaries.with_columns(
        conventional_heating_set_point=pl.lit(c_hs),
        actual_heating_set_point=pl.lit(a_hs),
    )
    solar_exposed_boundaries = boundaries.filter(
        pl.col("type").is_in([EXTERIOR_WALL, ROOF])
    ).with_row_index()
    other_boundaries = boundaries.filter(pl.col("type").is_in([INTERIOR_WALL, FLOOR]))
    direct_radiation = mask_influence(solar_exposed_boundaries, climate)

    direct_radiation, angle_of_incidence = radiation_on_boundary_model(
        solar_exposed_boundaries, direct_radiation, climate
    )

    heating_period_mask = heating_periods(climate, parameters)
    solar_exposed_boundaries = transmission_model(
        solar_exposed_boundaries,
        direct_radiation,
        angle_of_incidence,
        heating_period_mask,
    )
    solar_exposed_boundaries = solar_exposed_boundaries.drop("index")
    boundaries = pl.concat(
        [
            solar_exposed_boundaries,
            other_boundaries.with_columns(
                window_area=pl.lit(0.0),
                opaque_area=pl.col("area"),
                transmitted_solar_gain=pl.lit(0.0),
            ),
        ]
    )
    return boundaries
