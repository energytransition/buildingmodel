import polars as pl


def run_models(buildings, parameters):
    """

    Args:
        buildings:
        parameters:

    Returns:

    """
    expr_dict = {}
    for value_type in ["annual", "conventional"]:
        f_expr = []
        p_expr = []
        for energy, conversion_factor in parameters.primary_energies.items():
            f_expr.append(pl.col(f"{value_type}_{energy}_consumption"))
            p_expr.append(
                pl.col(f"{value_type}_{energy}_consumption") * conversion_factor
            )
        expr_dict[f"{value_type}_final_consumption"] = sum(f_expr)
        expr_dict[f"{value_type}_primary_consumption"] = sum(p_expr)
        expr_dict[f"{value_type}_final_consumption_by_surface"] = sum(f_expr) / pl.col(
            "living_area"
        )
        expr_dict[f"{value_type}_primary_consumption_by_surface"] = sum(
            p_expr
        ) / pl.col("living_area")

    buildings = buildings.with_columns(**expr_dict)

    return buildings
