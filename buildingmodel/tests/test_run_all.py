from pathlib import Path

from buildingmodel import data_path
from buildingmodel.main import Parameters, Simulation
from buildingmodel.logger import create_logger

step = "run_all"
logger = create_logger(file_name=f"{step}_test")
files = list((data_path["gis"] / "testing").glob("*.parquet"))


def process_file(file_path):
    parameters = Parameters(
        climate_folder=Path(data_path["climate"]) / "FRA_2004-2018",
    )
    simulation = Simulation(file_path, climate_data=None, parameters=parameters)
    simulation.run_all()


def make_test_func(filename):
    def test_func():
        process_file(filename)

    return test_func


for filename in files:
    test_name = f"test_{step}_{filename.stem}"
    globals()[test_name] = make_test_func(filename)
