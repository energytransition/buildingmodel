import copy
from multiprocessing import Pool

import numpy as np
import pandas as pd
from tqdm import tqdm
import geopandas as gpd
from buildingmodel import data_path
from pathlib import Path

cooking_inferred_vars = ["main_cooking_energy", "secondary_cooking_energy"]

cooking_input_vars = [
    "occupant_status",
    "living_area_class",
    "residential_type",
    "construction_year_class",
    "cmbl",
    "chfl",
]


def load_census_table():
    """

    :param table_type:
    :return:
    """

    cols_to_read = [
        "COMMUNE",
        "CATL",
        "BATI",
        "TYPL",
        "IPONDL",
        "IRIS",
        "CHFL",
        "CMBL",
        "ACHL",
        "STOCD",
        "INPER",
        "SURF",
    ]

    census_table = pd.read_parquet(
        Path(data_path["census"]) / "RP2021_logemt.parquet", columns=cols_to_read
    )

    census_table.columns = census_table.columns.str.lower()

    return census_table


def filter_by_type(dwellings):
    """
    Filtering dwelling according to their type:
        - on 'BATI' keep 'logements ordinaire France métropolitaine' (Z value)
        - on 'TYPL' keep only 'house' and 'apa

    :param dwellings:
    :return:
    """

    dwellings = dwellings.loc[dwellings["bati"] == "Z", :]
    dwellings = dwellings.loc[dwellings["typl"].isin(["1", "2"]), :]

    return dwellings


def create_common_iris(dwellings):
    """
    An iris corresponding to a whole municipality is marked 'ZZZZZZZZZ'.
    We replace it with 'COMMUNE' + '0000' to obtain a consistent iris notation.

    :param dwellings:
    :return:
    """

    na_iris = dwellings["iris"] == "ZZZZZZZZZ"

    dwellings.loc[na_iris, "iris"] = dwellings.loc[na_iris, "commune"] + "0000"

    return dwellings


def set_construction_year_class(dwellings):
    """
    building construction year class definition :
    1. [1000, 1918],
    2. [1919, 1945],
    3. [1946, 1970],
    4. [1971, 1990],
    5. [1991, 2005],
    6. [2006, 2012],
    7. [2013, 2100],

    :param dwellings:
    :return:
    """

    # construction period of the building, we group year 2006-2017 in one category
    dwellings["achl"] = dwellings["achl"].str.rstrip()

    ACHL_index = {"A11": 1, "A12": 2, "B11": 3, "B12": 4, "C100": 5}

    ACHL_index.update({f"C{i}": 6 for i in range(106, 113)})
    ACHL_index.update({f"C{i}": 7 for i in range(113, 119)})
    ACHL_index.update({f"C{i}": 7 for i in range(2019, 2024)})
    dwellings["achl"] = dwellings["achl"].replace(ACHL_index)
    print(dwellings.achl.unique())

    return dwellings


def set_heating_system(dwellings):
    """

    1. 'Chauffage électrique',
    2. 'Chaudière gaz réseau',
    3. 'Chaudière gaz citerne',
    4. 'Chaudière fioul',
    5. 'Chauffage urbain',
    6. 'Pompes à chaleur électricité',
    7. 'Chaudière - bois/charbon',
    8. 'Poêle - bois/charbon',
    9. 'Poêle - gaz réseau',
    10. 'Poêle - gaz citerne',
    11. 'Poêle - fioul',


    :param dwellings:
    :return:
    """

    dwellings["ch"] = 0
    dwellings.loc[dwellings["chfl"] == "3", "ch"] = 1
    dwellings.loc[dwellings["cmbl"] == "1", "ch"] = 5
    dwellings.loc[
        (dwellings["chfl"].isin(["1", "2"])) & (dwellings["cmbl"] == "2"), "ch"
    ] = 2
    dwellings.loc[
        (dwellings["chfl"].isin(["1", "2"])) & (dwellings["cmbl"] == "5"), "ch"
    ] = 3
    dwellings.loc[
        (dwellings["chfl"].isin(["1", "2"])) & (dwellings["cmbl"] == "3"), "ch"
    ] = 4
    dwellings.loc[
        (dwellings["chfl"].isin(["1", "2", "4"])) & (dwellings["cmbl"] == "4"), "ch"
    ] = 6
    dwellings.loc[
        (dwellings["chfl"].isin(["1", "2"])) & (dwellings["cmbl"] == "6"), "ch"
    ] = 7
    dwellings.loc[
        (dwellings["chfl"].isin(["4"])) & (dwellings["cmbl"] == "6"), "ch"
    ] = 8
    dwellings.loc[
        (dwellings["chfl"].isin(["4"])) & (dwellings["cmbl"] == "2"), "ch"
    ] = 9
    dwellings.loc[
        (dwellings["chfl"].isin(["4"])) & (dwellings["cmbl"] == "5"), "ch"
    ] = 10
    dwellings.loc[
        (dwellings["chfl"].isin(["4"])) & (dwellings["cmbl"] == "3"), "ch"
    ] = 11
    dwellings.loc[(dwellings["chfl"] == "Y"), "chfl"] = "0"
    dwellings.loc[(dwellings["cmbl"] == "Y"), "cmbl"] = "0"

    return dwellings


def set_categorical_values(dwellings):
    """

    :return:
    """
    rename_dicts = {
        "catl": {
            "1": "primary residence",
            "2": "occasional housing",
            "3": "second home",
            "4": "vacant dwelling",
        },
        "surf": {
            "1": "inf 30 m²",
            "2": "30 to 40 m²",
            "3": "40 to 60 m²",
            "4": "60 to 80 m²",
            "5": "80 to 100 m²",
            "6": "100 to 120 m²",
            "7": "sup 120 m²",
            "Y": "sup 120 m²",
        },
        "typl": {
            "1": "house",
            "2": "apartment",
            "3": "other",
            "4": "hotel room",
            "5": "other",
            "6": "other",
        },
        "achl": {
            1: "[1000, 1918]",
            2: "[1919, 1945]",
            3: "[1946, 1970]",
            4: "[1971, 1990]",
            5: "[1991, 2005]",
            6: "[2006, 2012]",
            7: "[2013, 2100]",
        },
        "stocd": {
            "10": "owner",
            "0": "non primary residence",
            "21": "renter",
            "23": "renter",
            "22": "low rent housing",
            "30": "free accomodation",
        },
        "ch": {
            0: "unknown",
            1: "electric heater",
            2: "fossil gas boiler",
            3: "liquified petroleum gas boiler",
            4: "oil boiler",
            5: "urban heat network",
            6: "electric heat pump",
            7: "biomass/coal boiler",
            8: "biomass/coal stove",
            9: "fossil gas stove",
            10: "liquified petroleum gas stove",
            11: "oil stove",
        },
    }

    dwellings.loc[:, "iris"] = dwellings["iris"].astype(str).astype("category")
    dwellings.loc[dwellings["inper"] == "Y", "inper"] = 0
    dwellings.loc[:, "inper"] = dwellings["inper"].astype(int)
    dwellings.loc[dwellings["inper"] > 6, "inper"] = 6

    for col, name_dict in rename_dicts.items():
        dwellings[col] = dwellings[col].replace(rename_dicts[col])
        dwellings.loc[:, col] = dwellings[col].astype("category")

    dwellings.loc[:, "cmbl"] = dwellings["cmbl"].astype(str).astype("category")
    dwellings.loc[:, "chfl"] = dwellings["chfl"].astype(str).astype("category")

    return dwellings


def group_iris_level(dwellings):
    """

    :param dwellings:
    :return:
    """
    columns_no_iris = [
        "achl",
        "catl",
        "ch",
        "cmbl",
        "chfl",
        "inper",
        "surf",
        "typl",
        "stocd",
        "ipondl",
    ]
    dwellings = dwellings.groupby(
        columns_no_iris + ["iris"], as_index=False, observed=True
    )["ipondl"].sum()
    print(dwellings.columns)

    return dwellings


def select_data_to_keep(dwellings):
    """

    :param dwellings:
    :return:
    """

    cols_to_keep = [
        "achl",
        "catl",
        "ch",
        "cmbl",
        "chfl",
        "inper",
        "surf",
        "typl",
        "stocd",
        "iris",
        "ipondl",
    ]

    rename_dict = {
        "catl": "occupancy_type",
        "stocd": "occupant_status",
        "surf": "living_area_class",
        "inper": "occupant_count",
        "ch": "heating_system",
        "typl": "residential_type",
        "achl": "construction_year_class",
        "iris": "district",
    }

    dwellings = dwellings.loc[:, cols_to_keep]
    dwellings.rename(columns=rename_dict, inplace=True)

    return dwellings


def load_survey_table():
    """

    :param table_type:
    :return:
    """

    survey_table = pd.read_csv(
        Path(data_path["census"]) / "dwelling_survey_variables.csv"
    )
    survey_table.dropna(subset=["cmbl", "chfl"], inplace=True)
    survey_table["cmbl"] = (
        survey_table["cmbl"].astype(int).astype(str).astype("category")
    )
    survey_table["chfl"] = (
        survey_table["chfl"].astype(int).astype(str).astype("category")
    )

    return survey_table


def chained_conditions(*conditions):
    return np.logical_and.reduce(conditions)


def multiple_filter(df, condition_dict):
    """

    Args:
        df:
        conditions:

    Returns:

    """
    if len(condition_dict) == 0:
        return df
    else:
        return df.loc[
            np.logical_and.reduce(
                [df[key] == val for key, val in condition_dict.items()]
            ),
            :,
        ]


def run_inference(dwellings, survey_dwellings, input_vars, inferred_vars, position=0):
    """

    Args:
        dwellings:
        survey_dwellings:

    Returns:

    """

    # we group the dwellings according to their input_vars and the district and draw in the  corresponding sample
    # in the census data to allocate census_vars to the dwellings
    current_input_vars = copy.copy(input_vars)

    dwelling_groups = dwellings.groupby(current_input_vars, observed=True).groups
    dwellings.loc[:, inferred_vars] = None

    for group, indices in tqdm(
        dwelling_groups.items(),
        total=len(dwelling_groups),
        position=position,
        dynamic_ncols=True,
    ):
        dwellings_condition_dict = {}
        for i, input_var in enumerate(current_input_vars):
            if not pd.isna(group[i]):
                dwellings_condition_dict[input_var] = group[i]

        survey_sample = multiple_filter(survey_dwellings, dwellings_condition_dict)

        if survey_sample.shape[0] == 0:
            for var in input_vars:
                del dwellings_condition_dict[var]
                survey_sample = multiple_filter(
                    survey_dwellings, dwellings_condition_dict
                )
                if survey_sample.shape[0] > 0:
                    break

        inferred_survey_id = np.random.choice(survey_sample.index, size=len(indices))

        inferred_survey = survey_sample.loc[inferred_survey_id, :]
        for var in inferred_vars:
            dwellings.loc[indices, var] = inferred_survey[var].values

    return dwellings


def set_categorical_values_after_survey(dwellings, inferred_vars):
    for col in inferred_vars:
        dwellings.loc[:, col] = dwellings[col].astype("category")

    return dwellings


def select_data_to_keep_after_survey(dwellings):
    """

    :param dwellings:
    :return:
    """

    cols_to_keep = [
        "occupancy_type",
        "occupant_status",
        "living_area_class",
        "occupant_count",
        "heating_system",
        "residential_type",
        "construction_year_class",
        "district",
        "main_cooking_energy",
        "secondary_cooking_energy",
        "ipondl",
    ]

    dwellings = dwellings.loc[:, cols_to_keep]

    return dwellings


def split_census_table(dwellings):
    primary_mask = dwellings["occupancy_type"] == "primary residence"
    dwellings_primary = dwellings.loc[primary_mask]
    dwellings_other = dwellings.loc[~primary_mask]

    return dwellings_primary, dwellings_other


def multi_run_inference(args):
    return run_inference(**args)


def run_parallel_inference(
    census_other, census_primary, input_vars, inferred_vars, n_cpu
):
    df_split = np.array_split(census_other, n_cpu)

    p = Pool(processes=n_cpu)
    data = p.map(
        multi_run_inference,
        [
            {
                "dwellings": df,
                "survey_dwellings": census_primary,
                "input_vars": input_vars,
                "inferred_vars": inferred_vars,
                "position": i,
            }
            for i, df in enumerate(df_split)
        ],
    )

    p.close()

    return pd.concat(data, axis=0)


def run_second_home_inference(dwellings):
    inferred_vars = [
        "main_cooking_energy",
        "secondary_cooking_energy",
        "heating_system",
    ]

    input_vars = [
        "residential_type",
        "construction_year_class",
        "district",
    ]
    dwellings_primary, dwellings_other = split_census_table(dwellings)
    dwellings_other = run_parallel_inference(
        dwellings_other, dwellings_primary, input_vars, inferred_vars, n_cpu=16
    )

    dwellings = pd.concat([dwellings_primary, dwellings_other], axis=0)

    return dwellings


def run():
    print("load census")
    dwellings = load_census_table()
    print("filter by type")
    dwellings = filter_by_type(dwellings)
    print("create iris")
    dwellings = create_common_iris(dwellings)
    print("set construction year class")
    dwellings = set_construction_year_class(dwellings)
    print("set heating system")
    dwellings = set_heating_system(dwellings)
    print("set categorical values")
    dwellings = set_categorical_values(dwellings)
    print("group at iris level")
    dwellings = group_iris_level(dwellings)
    dwellings = select_data_to_keep(dwellings)

    print("load survey")
    survey_dwellings = load_survey_table()

    dwellings_primary = dwellings.loc[dwellings.occupancy_type == "primary residence"]
    dwellings_other = dwellings.loc[dwellings.occupancy_type != "primary residence"]
    print("run inference of cooking energy")
    dwellings_primary = run_inference(
        dwellings_primary, survey_dwellings, cooking_input_vars, cooking_inferred_vars
    )

    print("second home inference")
    dwellings = pd.concat([dwellings_primary, dwellings_other], axis=0)
    dwellings = run_second_home_inference(dwellings)

    dwellings = set_categorical_values_after_survey(dwellings, cooking_inferred_vars)
    dwellings = select_data_to_keep_after_survey(dwellings)

    return dwellings


if __name__ == "__main__":
    dwellings = run()

    districts = pd.read_parquet(
        Path(data_path["gis"]) / "districts.parquet",
        columns=["district", "city", "city_group", "department", "region"],
    )
    dwellings = dwellings.merge(districts, on="district", how="left")
    cols_cat = [
        "occupancy_type",
        "occupant_status",
        "living_area_class",
        "heating_system",
        "residential_type",
        "construction_year_class",
        "district",
        "main_cooking_energy",
        "secondary_cooking_energy",
        "city",
        "city_group",
        "department",
        "region",
    ]
    dwellings = dwellings.astype({c: "category" for c in cols_cat})
    dwellings.to_parquet(Path(data_path["census"]) / "processed_census_2021.parquet")
