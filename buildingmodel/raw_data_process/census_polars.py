import copy
from multiprocessing import Pool
from buildingmodel.inference.dwelling import join_random_by_columns
from buildingmodel.logger import create_logger
import numpy as np
import pandas as pd
import polars as pl

pl.enable_string_cache()
from tqdm import tqdm
import geopandas as gpd
from buildingmodel import data_path
from pathlib import Path

cooking_inferred_vars = ["main_cooking_energy", "secondary_cooking_energy"]

cooking_input_vars = [
    "occupant_status",
    "living_area_class",
    "cmbl",
    "residential_type",
    "construction_year_class_simplified",
]

logger = create_logger("census_preprocess")


def load_census_table():
    """

    :param table_type:
    :return:
    """

    cols_to_read = [
        "COMMUNE",
        "CATL",
        "BATI",
        "TYPL",
        "IPONDL",
        "IRIS",
        "CHFL",
        "CMBL",
        "ACHL",
        "STOCD",
        "INPER",
        "SURF",
    ]

    census_table = (
        pl.scan_parquet(Path(data_path["census"]) / "RP2021_logemt.parquet")
        .select(cols_to_read)
        .collect()
    )

    census_table.columns = [c.lower() for c in census_table.columns]

    return census_table


def filter_by_type(dwellings):
    """
    Filtering dwelling according to their type:
        - on 'BATI' keep 'logements ordinaire France métropolitaine' (Z value)
        - on 'TYPL' keep only 'house' and 'apa

    :param dwellings:
    :return:
    """

    return dwellings.filter(
        (pl.col("bati") == "Z") & (pl.col("typl").is_in(["1", "2"]))
    )


def create_common_iris(dwellings):
    """
    An iris corresponding to a whole municipality is marked 'ZZZZZZZZZ'.
    We replace it with 'COMMUNE' + '0000' to obtain a consistent iris notation.

    :param dwellings:
    :return:
    """

    na_iris = dwellings["iris"] == "ZZZZZZZZZ"

    dwellings = dwellings.with_columns(
        iris=pl.when(na_iris).then(pl.col("commune") + "0000").otherwise(pl.col("iris"))
    )

    return dwellings


def set_construction_year_class(dwellings):
    """
    building construction year class definition :
    1. [1000, 1918],
    2. [1919, 1945],
    3. [1946, 1970],
    4. [1971, 1990],
    5. [1991, 2005],
    6. [2006, 2012],
    7. [2013, 2100],

    :param dwellings:
    :return:
    """

    # construction period of the building, we group year 2006-2017 in one category
    dwellings = dwellings.with_columns(pl.col("achl").str.strip_chars())

    ACHL_index = {"A11": 1, "A12": 2, "B11": 3, "B12": 4, "C100": 5}

    ACHL_index.update({f"C{i}": 6 for i in range(106, 113)})
    ACHL_index.update({f"C{i}": 7 for i in range(113, 119)})
    ACHL_index.update({f"C{i}": 7 for i in range(2019, 2024)})
    dwellings = dwellings.with_columns(
        achl=dwellings["achl"].replace_strict(ACHL_index)
    )

    return dwellings


def set_heating_system(dwellings):
    """

    1. 'Chauffage électrique',
    2. 'Chaudière gaz réseau',
    3. 'Chaudière gaz citerne',
    4. 'Chaudière fioul',
    5. 'Chauffage urbain',
    6. 'Pompes à chaleur électricité',
    7. 'Chaudière - bois/charbon',
    8. 'Poêle - bois/charbon',
    9. 'Poêle - gaz réseau',
    10. 'Poêle - gaz citerne',
    11. 'Poêle - fioul',


    :param dwellings:
    :return:
    """

    dwellings = dwellings.with_columns(
        ch=(
            pl.when(pl.col("chfl") == "3")
            .then(pl.lit(1))
            .when(pl.col("cmbl") == "1")
            .then(pl.lit(5))
            .when(pl.col("chfl").is_in(["1", "2"]) & (pl.col("cmbl") == "2"))
            .then(pl.lit(2))
            .when(pl.col("chfl").is_in(["1", "2"]) & (pl.col("cmbl") == "5"))
            .then(pl.lit(3))
            .when(pl.col("chfl").is_in(["1", "2"]) & (pl.col("cmbl") == "3"))
            .then(pl.lit(4))
            .when(pl.col("chfl").is_in(["1", "2", "4"]) & (pl.col("cmbl") == "4"))
            .then(pl.lit(6))
            .when(pl.col("chfl").is_in(["1", "2"]) & (pl.col("cmbl") == "6"))
            .then(pl.lit(7))
            .when((pl.col("chfl") == "4") & (pl.col("cmbl") == "6"))
            .then(pl.lit(8))
            .when((pl.col("chfl") == "4") & (pl.col("cmbl") == "2"))
            .then(pl.lit(9))
            .when((pl.col("chfl") == "4") & (pl.col("cmbl") == "5"))
            .then(pl.lit(10))
            .when((pl.col("chfl") == "4") & (pl.col("cmbl") == "3"))
            .then(pl.lit(11))
            .otherwise(pl.lit(0))
        )
    )

    return dwellings


def set_categorical_values(dwellings):
    """

    :return:
    """
    rename_dicts = {
        "catl": {
            "1": "primary residence",
            "2": "occasional housing",
            "3": "second home",
            "4": "vacant dwelling",
        },
        "surf": {
            "1": "inf 30 m²",
            "2": "30 to 40 m²",
            "3": "40 to 60 m²",
            "4": "60 to 80 m²",
            "5": "80 to 100 m²",
            "6": "100 to 120 m²",
            "7": "sup 120 m²",
            "Y": "sup 120 m²",
        },
        "typl": {
            "1": "house",
            "2": "apartment",
            "3": "other",
            "4": "hotel room",
            "5": "other",
            "6": "other",
        },
        "achl": {
            1: "[1000, 1918]",
            2: "[1919, 1945]",
            3: "[1946, 1970]",
            4: "[1971, 1990]",
            5: "[1991, 2005]",
            6: "[2006, 2012]",
            7: "[2013, 2100]",
        },
        "stocd": {
            "10": "owner",
            "0": "non primary residence",
            "21": "renter",
            "23": "renter",
            "22": "low rent housing",
            "30": "free accomodation",
        },
        "ch": {
            0: "unknown",
            1: "electric heater",
            2: "fossil gas boiler",
            3: "liquified petroleum gas boiler",
            4: "oil boiler",
            5: "urban heat network",
            6: "electric heat pump",
            7: "biomass/coal boiler",
            8: "biomass/coal stove",
            9: "fossil gas stove",
            10: "liquified petroleum gas stove",
            11: "oil stove",
        },
    }

    dwellings = dwellings.with_columns(
        pl.col("iris").cast(pl.String).cast(pl.Categorical),
        pl.col("cmbl").cast(pl.String).cast(pl.Categorical),
        pl.col("chfl").cast(pl.String).cast(pl.Categorical),
        inper=pl.when(pl.col("inper") == "Y")
        .then(pl.lit(0))
        .otherwise(pl.col("inper"))
        .cast(pl.Int32),
        **{
            k: dwellings[k].replace_strict(v, default="unknown").cast(pl.Categorical)
            for k, v in rename_dicts.items()
        },
    )

    dwellings = dwellings.with_columns(
        construction_year_class_simplified=(
            dwellings["achl"]
            .cast(pl.String)
            .replace(
                {
                    "[2006, 2012]": "[2006, 2100]",
                    "[2013, 2100]": "[2006, 2100]"
                }
        ).cast(pl.Categorical),
        )[0]
    )

    return dwellings


def group_iris_level(dwellings):
    """

    :param dwellings:
    :return:
    """
    columns_no_iris = [
        "achl",
        "catl",
        "ch",
        "cmbl",
        "chfl",
        "inper",
        "surf",
        "typl",
        "stocd",
        "construction_year_class_simplified"
    ]
    dwellings = dwellings.group_by(columns_no_iris + ["iris"]).agg(
        pl.col("ipondl").sum()
    )

    return dwellings


def select_data_to_keep(dwellings):
    """

    :param dwellings:
    :return:
    """

    cols_to_keep = [
        "achl",
        "catl",
        "ch",
        "cmbl",
        "chfl",
        "inper",
        "surf",
        "typl",
        "stocd",
        "iris",
        "ipondl",
        "construction_year_class_simplified",
    ]

    rename_dict = {
        "catl": "occupancy_type",
        "stocd": "occupant_status",
        "surf": "living_area_class",
        "inper": "occupant_count",
        "ch": "heating_system",
        "typl": "residential_type",
        "achl": "construction_year_class",
        "iris": "district",
    }

    dwellings = dwellings.select(cols_to_keep)
    dwellings = dwellings.rename(rename_dict)

    return dwellings


def load_survey_table():
    """

    :param table_type:
    :return:
    """

    survey_table = pl.read_csv(
        Path(data_path["census"]) / "dwelling_survey_variables.csv"
    )
    survey_table = survey_table.drop_nans(subset=["cmbl", "chfl"]).with_columns(
        pl.col("cmbl").cast(pl.Int32),
        pl.col("chfl").cast(pl.Int32),
        pl.col("living_area_class").str.replace_many([">", "<"], ["sup", "inf"]),
        construction_year_class_simplified=pl.col("construction_year_class")
    )

    survey_table = survey_table.with_columns(
        [
            pl.col(c).cast(pl.String).cast(pl.Categorical)
            for c in [cooking_input_vars + cooking_inferred_vars]
        ]
    )
    return survey_table


def set_categorical_values_after_survey(dwellings, inferred_vars):
    dwellings = dwellings.with_columns(
        [pl.col(c).cast(pl.Categorical) for c in inferred_vars]
    )

    return dwellings


def select_data_to_keep_after_survey(dwellings):
    """

    :param dwellings:
    :return:
    """

    cols_to_keep = [
        "occupancy_type",
        "occupant_status",
        "living_area_class",
        "occupant_count",
        "heating_system",
        "residential_type",
        "construction_year_class",
        "district",
        "main_cooking_energy",
        "secondary_cooking_energy",
        "ipondl",
    ]

    dwellings = dwellings.select(cols_to_keep)

    return dwellings


def run_second_home_inference(dwellings):
    inferred_vars = [
        "main_cooking_energy",
        "secondary_cooking_energy",
        "heating_system",
    ]

    input_vars = ["residential_type", "construction_year_class", "district", "city", "department"]

    dwellings = dwellings.with_columns(
        city=pl.col("district").cast(pl.String).str.slice(0, 5),
        department=pl.col("district").cast(pl.String).str.slice(0, 2)
    )

    join_columns = [input_vars[i:] for i in range(len(input_vars))]
    dwellings_primary = dwellings.filter(
        pl.col("occupancy_type") == "primary residence"
    )
    dwellings_other = dwellings.filter(pl.col("occupancy_type") != "primary residence").drop(inferred_vars)
    dwellings_other = join_random_by_columns(
        dwellings_other,
        dwellings_primary[input_vars + inferred_vars],
        join_columns,
        join_quantile=None,
        to_logger=True,
    )

    dwellings = pl.concat([dwellings_primary,
                           dwellings_other.select(dwellings_primary.columns)], how="vertical")

    return dwellings


def run():
    print("load census")
    dwellings = load_census_table()
    print("filter by type")
    dwellings = filter_by_type(dwellings)
    print("create iris")
    dwellings = create_common_iris(dwellings)
    print("set construction year class")
    dwellings = set_construction_year_class(dwellings)
    print("set heating system")
    dwellings = set_heating_system(dwellings)
    print("set categorical values")
    dwellings = set_categorical_values(dwellings)
    print("group at iris level")
    dwellings = group_iris_level(dwellings)
    dwellings = select_data_to_keep(dwellings)

    print("load survey")
    survey_dwellings = load_survey_table()

    dwellings_primary = dwellings.filter(
        pl.col("occupancy_type") == "primary residence"
    )
    dwellings_other = dwellings.filter(pl.col("occupancy_type") != "primary residence")
    print("run inference of cooking energy")
    join_columns = [cooking_input_vars[i:] for i in range(len(cooking_input_vars))]
    dwellings_primary = join_random_by_columns(
        dwellings_primary,
        survey_dwellings[cooking_input_vars + cooking_inferred_vars],
        join_columns,
        join_quantile=None,
        to_logger=True,
    )

    print("second home inference")
    dwellings = pl.concat([dwellings_primary, dwellings_other], how="diagonal")
    dwellings = run_second_home_inference(dwellings)

    dwellings = set_categorical_values_after_survey(dwellings, cooking_inferred_vars)
    dwellings = select_data_to_keep_after_survey(dwellings)

    return dwellings


if __name__ == "__main__":
    dwellings = run()

    districts = pl.from_pandas(
        pd.read_parquet(
            Path(data_path["gis"]) / "districts.parquet",
            columns=["district", "city", "city_group", "department", "region"],
        )
    )
    districts = districts.with_columns(pl.col("district").cast(pl.Categorical))
    dwellings = dwellings.join(districts, on="district", how="left")
    cols_cat = [
        "occupancy_type",
        "occupant_status",
        "living_area_class",
        "heating_system",
        "residential_type",
        "construction_year_class",
        "district",
        "main_cooking_energy",
        "secondary_cooking_energy",
        "city",
        "city_group",
        "department",
        "region",
    ]
    dwellings = dwellings.with_columns(
        [pl.col(c).cast(pl.Categorical) for c in cols_cat]
    )
    dwellings.write_parquet(Path(data_path["census"]) / "processed_census_2021.parquet")
