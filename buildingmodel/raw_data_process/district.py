import pandas as pd
from pathlib import Path
from buildingmodel import data_path
import geopandas as gpd

raw_iris_path = Path(data_path["gis"]) / "raw_districts"
import time

DEFAULT_MISSING_CITY_GROUPS = {
    "Paris": {"EPCI": "75000", "DEP": "75", "REG": "11"},
    "Marseille": {"EPCI": "13000", "DEP": "13", "REG": "93"},
    "Lyon": {"EPCI": "69000", "DEP": "69", "REG": "84"},
    "Les Trois Lacs": {"EPCI": "200072312", "DEP": "27", "REG": "28"},
}
DEFAULT_ADMIN_VALUES = ["INSEE_COM", "CODE_IRIS", "EPCI", "DEP", "REG"]


def load_iris_boundaries():
    """
    This function loads the GIS file containing the sub-municipality administrative boundaries

    :param iris_file: location of the sub-municipality administrative boundary file
    :return: a geopandas GeoDataframe containing the sub-municipality administrative boundaries
    """

    print(f"Start reading iris boundary data")
    start_time = time.time()
    iris_boundary_gdf = gpd.read_file(
        raw_iris_path / "contours-iris.gpkg", engine="pyogrio"
    ).set_crs("EPSG:2154")
    duration = time.time() - start_time
    print(
        f"Finished reading iris boundary data. Elapsed time : {duration:4.1f}, Number of records :"
        f" {iris_boundary_gdf.shape[0]}"
    )

    return iris_boundary_gdf


def load_epci_data():
    """
    This function loads an inter-municipality data file

    :param epci_file: location of the data file
    :return: a pandas Dataframe containing the inter-municipality data
    """

    epci_data = pd.read_excel(
        raw_iris_path / "Intercommunalite_Metropole_au_01-01-2022.xlsx",
        sheet_name=1,
        engine="openpyxl",
        header=5,
        usecols=range(6),
    )

    return epci_data


def load_ept_data():
    """
    This function loads an intermunicipality data file

    :param ept_file: location of the ept data file
    :return: a pandas Dataframe containing the inter-municipality data
    """

    ept_data = pd.read_excel(
        raw_iris_path / "ept_au_01-01-2023.xlsx",
        sheet_name=1,
        engine="openpyxl",
        header=5,
        usecols=range(6),
    )

    return ept_data


def set_missing_city_groups(iris_with_city_group, missing_city_groups):
    """

    :param iris_with_city_group: geopandas GeoDataframe containing the IRIS polygons with inter-municipality data
    :param missing_city_groups: custom definition of EPCI mainly for cities with Arrondissements (Paris, Marseille, Lyon)
    :return: geopandas GeoDataframe containing the IRIS polygons with inter-municipality data
    """

    for city_name, city_groups in missing_city_groups.items():
        mask = (
            iris_with_city_group.EPCI.isna()
            & iris_with_city_group.nom_commune.str.contains(city_name)
        )

        for col_name, value in city_groups.items():
            iris_with_city_group.loc[mask, col_name] = value

    missing_city_group_count = iris_with_city_group.EPCI.isna().sum()
    print(f"Missing city groups : {missing_city_group_count}")

    return iris_with_city_group


def merge_iris_city_groups(iris_boundaries, city_group_df):
    """
    this function merges the sub-municipality administrative boundaries (IRIS) with the inter-municipality data (EPCI, DEP
    REG)

    :param iris_boundaries: geopandas GeoDataframe containing the IRIS polygons
    :param city_group_df: pandas Dataframe containing inter-municipality data
    :return: geopandas GeoDataframe containing the IRIS polygons with inter-municipality data
    """

    return iris_boundaries.merge(
        city_group_df, left_on="code_insee", right_on="CODGEO", how="left"
    )


if __name__ == "__main__":
    iris = load_iris_boundaries()
    epci = load_epci_data()
    ept = load_ept_data()
    ept.rename(
        columns={
            "EPT": "EPCI",
        },
        inplace=True,
    )

    cols_to_keep = ["CODGEO", "EPCI", "DEP", "REG"]

    epci_data = pd.concat([epci[cols_to_keep], ept[cols_to_keep]], axis=0)
    epci_data["REG"] = epci_data["REG"].astype(int).astype(str)

    iris_with_epci_data = merge_iris_city_groups(iris, epci_data)
    iris_with_epci_data = set_missing_city_groups(
        iris_with_epci_data, DEFAULT_MISSING_CITY_GROUPS
    )

    rename_dict = {
        "EPCI": "city_group",
        "REG": "region",
        "DEP": "department",
        "code_iris": "district",
        "CODGEO": "city",
        "nom_commune": "city_name",
    }
    iris_with_epci_data = iris_with_epci_data.rename(columns=rename_dict)
    existing_district_data = gpd.read_file(
        Path(data_path["gis"]) / "districts_with_network_presence.gpkg"
    )
    iris_with_epci_data = iris_with_epci_data.merge(
        existing_district_data[
            ["district", "has_network_city_level", "has_network_grdf_data"]
        ],
        how="left",
        on="district",
    )
    iris_with_epci_data.to_parquet(
        raw_iris_path / "districts_with_city_group.parquet", write_covering_bbox=True
    )
