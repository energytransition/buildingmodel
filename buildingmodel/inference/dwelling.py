import gc

from buildingmodel import data_path
import pandas as pd
from pathlib import Path
import random
import numpy as np
import functools
import copy
from metalog import metalog
import itertools
import polars as pl

import logging

logger = logging.getLogger("buildingmodel")
from buildingmodel.logger import duration_logging

inferred_vars = [
    "occupancy_type",
    "occupant_count",
    "heating_system",
    "main_cooking_energy",
    "secondary_cooking_energy",
]

input_vars = ["residential_type", "construction_year_class"]

geo_levels = ["district", "city", "city_group", "department", "region"]

HOUSE_JOIN_COLUMNS = [
    [
        "construction_year_class",
        "living_area_class",
        "gas_network_connection",
        "district",
    ],
    ["construction_year_class", "gas_network_connection", "district"],
    ["construction_year_class", "living_area_class", "gas_network_connection", "city"],
    ["construction_year_class", "gas_network_connection", "city"],
    [
        "construction_year_class",
        "living_area_class",
        "gas_network_connection",
        "city_group",
    ],
    ["construction_year_class", "gas_network_connection", "city_group"],
    ["gas_network_connection", "city_group"],
]
APARTMENT_JOIN_COLUMNS = [
    ["construction_year_class", "gas_network_connection", "district"],
    ["construction_year_class", "gas_network_connection", "city"],
    ["construction_year_class", "gas_network_connection", "city_group"],
    ["gas_network_connection", "city_group"],
]

census_data = {}

living_area_classes = {
    "inf 30 m²": [10.0, 30.0],
    "30 to 40 m²": [30.0, 40.0],
    "40 to 60 m²": [40.0, 60.0],
    "60 to 80 m²": [60.0, 80.0],
    "80 to 100 m²": [80.0, 100.0],
    "100 to 120 m²": [100.0, 120.0],
    "sup 120 m²": [120.0, 150.0],
}

living_area_share_limit = {"house": 0.9, "apartment": 0.7}

floor_area_bins = {
    "house_only": [0.0, 80.0, 120.0, 180.0, 280.0, 500.0, 10000.0],
    "house_with_annex": [0.0, 150.0, 250.0, 350.0, 10000.0],
    "multiple_use": [0.0, 200.0, 400.0, 600.0, 100000.0],
}


def calculate_house_living_area(houses):
    """

    Returns:

    """
    regression_path = (
        Path(data_path["building_statistics"])
        / "living_area"
        / "house_living_area_regression.csv"
    )
    regression_df = pl.read_csv(regression_path).drop("score")
    houses = (
        houses.with_columns(
            pl.col("floor_area_share").clip(9.0, regression_df["max"].max())
        )
        .join(regression_df, how="cross")
        .filter(
            (pl.col("floor_area_share") >= pl.col("min"))
            & (pl.col("floor_area_share") <= pl.col("max"))
        )
        .with_columns(
            living_area=(
                pl.col("floor_area_share") * pl.col("coef") + pl.col("intercept")
            ).clip(9.0, pl.col("floor_area_share"))
        )
        .drop(["min", "max", "intercept", "coef"])
    )
    houses = set_house_living_area_class(houses)
    return houses


def calculate_apartment_living_area(dwellings):
    """
    For each apartment, the living area is drawn from a metalog distribution calibrated on a sample of dwellings from
    the same living area class according to the census.

    However, in many cases, this results in a total living area incompatible with the geometry of the building and their
    categories (with annex or not, multiple use). To correct this, buildings with living area shares outside of bounds
    defined by category are selected and the number of dwellings and their living area are modified. See
    correct_apartment_living_area for details.

    Args:
        dwellings:

    Returns:

    """
    metalog_path = Path(data_path["building_statistics"]) / "living_area"
    dwellings = dwellings.with_row_index("index")
    living_areas = np.zeros(dwellings.shape[0])

    for living_area_class, bounds in living_area_classes.items():
        fit_path = (
            metalog_path
            / f"metalog_fit_apartment_living_area_{living_area_class}.pickle"
        )
        with open(fit_path, "rb") as handle:
            metalog_fit = pd.compat.pickle_compat.load(
                handle
            )  # to ensure compatibility across pandas versions

        mask = dwellings["living_area_class"] == living_area_class
        dw_in_class = mask.sum()
        if dw_in_class > 0:
            living_areas[mask] = np.clip(
                metalog.r(
                    m=metalog_fit,
                    n=dw_in_class,
                    term=metalog_fit["params"]["term_limit"],
                ),
                *bounds,
            )

    dwellings = dwellings.with_columns(
        living_area=np.clip(
            living_areas, a_min=None, a_max=dwellings["floor_area_share"].to_numpy()
        ),
    ).drop("index")

    return dwellings


def building_level_living_area(buildings, dwellings):
    """
    Calculates the total living area and living area share at the building level

    Args:
        buildings (GeoDataframe): a GeoDataframe containing the building geometries and parameters
        dwellings: a Dataframe containing dwellings parameters and results

    Returns:

    """
    building_living_area = dwellings.groupby("building_id")["living_area"].sum()
    buildings.loc[building_living_area.index, "living_area"] = (
        building_living_area.values
    )
    buildings["living_area_share"] = buildings["living_area"] / buildings["floor_area"]


def set_house_living_area_class(houses):
    """
    Defines the living area class of houses according to the census catagories

    Args:
        dwellings: a Dataframe containing dwellings parameters and results

    Returns:

    """

    bins = list(
        dict.fromkeys(itertools.chain.from_iterable(living_area_classes.values()))
    )[1:-1]
    labels = list(living_area_classes.keys())

    return houses.with_columns(
        living_area_class=pl.col("living_area").cut(breaks=bins, labels=labels)
    )


def set_occupant_count(dwellings):
    """
    Sets the occupant count from categories.
    Sets the occupant count for second homes to 2.

    Args:
        dwellings: a Dataframe containing dwellings parameters and results

    Returns:

    """

    dwellings = dwellings.with_columns(
        pl.col("occupant_count").cast(pl.String)
    ).with_columns(
        occupant_count=pl.when(pl.col("occupant_count") == ">6")
        .then(pl.lit("6"))
        .when(pl.col("occupancy_type") == "second home")
        .then(pl.lit("2"))
        .otherwise(pl.col("occupant_count"))
        .cast(pl.Int32)
    )

    return dwellings


def get_group_size_index(data, join_columns, weight_column=None, prefix=""):
    agg_exprs = [
        pl.col("index").alias(f"{prefix}_index"),
    ]
    if weight_column is not None:
        agg_exprs += [pl.col(weight_column).sum().alias(f"{prefix}_count")]
    else:
        agg_exprs += [pl.len().alias(f"{prefix}_count")]

    return data.group_by(join_columns).agg(agg_exprs)


def join_and_select_groups(data_group, data_to_join_group, join_columns, join_quantile):
    """

    :param data_group:
    :param data_to_join_group:
    :return:
    """

    joined_groups = data_to_join_group.join(data_group, on=join_columns, how="inner")
    joined_groups = joined_groups.with_columns(
        (pl.col("data_count") / pl.col("data_to_join_count")).alias("ratio")
    )

    if (join_quantile is None) or (joined_groups.shape[0] == 0):
        selected_groups = joined_groups
    else:
        max_ratio = joined_groups["ratio"].quantile(join_quantile)
        selected_groups = joined_groups.filter(pl.col("ratio") <= max_ratio)

    selected_groups = selected_groups.drop("ratio")

    return selected_groups


def assign_random_row(selected_groups):
    exploded_data_groups = selected_groups.explode("data_index")

    matched_rows = (
        exploded_data_groups.with_columns(
            pl.col("data_to_join_index")
            .list.eval(pl.element().sample())
            .alias("data_to_join_index")
        )
        .explode("data_to_join_index")
        .select(["data_index", "data_to_join_index"])
    )

    return matched_rows


def join_random_in_group(
    data,
    data_to_join,
    join_columns,
    join_quantile,
    left_weight_column=None,
    right_weight_column=None,
):
    """

    :param join_columns:
    :param min_share:
    :return:
    """

    data_group = get_group_size_index(
        data, join_columns, weight_column=left_weight_column, prefix="data"
    )
    data_to_join_group = get_group_size_index(
        data_to_join,
        join_columns,
        weight_column=right_weight_column,
        prefix="data_to_join",
    )

    selected_groups = join_and_select_groups(
        data_group, data_to_join_group, join_columns, join_quantile
    )

    if selected_groups.shape[0] == 0:
        return None

    matched_rows = assign_random_row(selected_groups)

    data_matched_rows = data.join(matched_rows, left_on="index", right_on="data_index")
    joined_data = data_matched_rows.join(
        data_to_join.drop(join_columns),
        left_on="data_to_join_index",
        right_on="index",
        how="left",
    )

    cols_to_drop = ["data_to_join_index"] + [
        col for col in joined_data.columns if "_right" in col
    ]
    joined_data = joined_data.drop(cols_to_drop)

    return joined_data


def join_random_by_columns(
    data,
    data_to_join,
    join_column_list,
    join_quantile,
    left_weight_column=None,
    right_weight_column=None,
    to_logger=True,
):
    data = data.with_row_index()
    data_to_join = data_to_join.with_row_index()

    data_count = data.shape[0]
    unjoined_data = data
    joined_data_list = []
    joined_count = 0

    for i, join_columns in enumerate(join_column_list):
        if i >= (len(join_column_list) - 2):
            join_quantile = None

        joined_data = join_random_in_group(
            unjoined_data,
            data_to_join,
            join_columns,
            join_quantile,
            left_weight_column,
            right_weight_column,
        )
        if joined_data is None:
            if to_logger:
                logger.info(
                    f"Level {str(i)} : {joined_count} of {data_count} records have been joined"
                )
            continue
        unjoined_data = unjoined_data.filter(
            ~pl.col("index").is_in(joined_data.select("index").to_series().to_list())
        )
        gc.collect()
        joined_data = joined_data.drop("index")
        joined_data = joined_data.with_columns(pl.lit(i).alias("merge_level"))
        joined_data_list.append(joined_data)
        joined_count += joined_data.shape[0]
        if to_logger:
            logger.info(
                f"Level {str(i)} : {joined_count} of {data_count} records have been joined"
            )
        if joined_count == data_count:
            break
    else:
        raise RuntimeError(
            f"Inference could not be completed for {data_count - joined_count} records"
        )

    return pl.concat(joined_data_list)


@duration_logging
def run_dwelling_inference(dwellings, buildings, parameters, census):
    """
    for each dwelling, select the records in the census database that matches the most closely the dwelling considering
    the construction year class, the residential type and the district. Applies the relevant
    parameters from the census to the dwelling

    Args:
        dwellings:
        parameters: instance of Parameters class containing simulation level parameters

    Returns:

    """
    dwellings = dwellings.join(
        buildings[["building_id", "gas_network_connection"]],
        how="left",
        on="building_id",
    ).with_columns(pl.col("residential_type").cast(pl.Categorical))

    dw_to_concat = []
    houses = dwellings.filter(pl.col("residential_type") == "house")
    if houses.shape[0] > 0:
        logger.info(f"Start of house inference by joining census with dwellings")
        houses = calculate_house_living_area(houses)
        houses = join_random_by_columns(
            houses,
            census,
            HOUSE_JOIN_COLUMNS,
            right_weight_column="ipondl",
            join_quantile=parameters.census_join_quantile,
        )
        dw_to_concat.append(houses)

    apartments = dwellings.filter(pl.col("residential_type") == "apartment")
    if apartments.shape[0] > 0:
        logger.info(f"Start of apartment inference by joining census with dwellings")
        apartments = join_random_by_columns(
            apartments,
            census,
            APARTMENT_JOIN_COLUMNS,
            right_weight_column="ipondl",
            join_quantile=parameters.census_join_quantile,
        )
        apartments = calculate_apartment_living_area(apartments)
        dw_to_concat.append(apartments)

    if len(dw_to_concat) > 1:
        dw_to_concat[1] = dw_to_concat[1].select(dw_to_concat[0].columns)
    dwellings = pl.concat(dw_to_concat, how="vertical")

    dwellings = set_occupant_count(dwellings)

    incompatible_cooking = (pl.col("main_cooking_energy") == "fossil_gas") & (
        pl.col("gas_network_connection") == False
    )
    dwellings = dwellings.with_columns(
        main_cooking_energy=pl.when(incompatible_cooking)
        .then(pl.lit("liquified_petroleum_gas"))
        .otherwise(pl.col("main_cooking_energy"))
    )

    resi_types = dwellings["residential_type"].unique()

    dw_summary = (
        dwellings.group_by(["residential_type", "heating_system"])
        .agg(count=pl.col("building_id").count())
        .pivot("residential_type", index="heating_system", values="count")
        .fill_null(0)
        .with_columns(total=sum([pl.col(r) for r in resi_types]))
        .sort(["total"], descending=True)
        .head(10)
    )
    logger.info(dw_summary)

    return dwellings, buildings
