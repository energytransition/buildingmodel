import polars as pl

from buildingmodel.inference.dwelling import join_random_by_columns
from buildingmodel.logger import duration_logging
import logging

logger = logging.getLogger("buildingmodel")

BUILDING_JOIN_COLUMNS = [
    [
        "construction_year_class",
        "residential_type",
        "heating_system",
        "district",
    ],
    [
        "construction_year_class",
        "residential_type",
        "heating_system",
        "city",
    ],
    [
        "construction_year_class",
        "residential_type",
        "heating_system",
        "city_group",
    ],
    [
        "residential_type",
        "heating_system",
        "city_group",
    ],
    [
        "construction_year_class",
        "residential_type",
        "heating_system",
    ],
    [
        "residential_type",
        "heating_system",
    ],
]


def log_summary(buildings):
    b_desc = buildings.select(
        [
            "wall_u_value",
            "roof_u_value",
            "floor_u_value",
            "wall_window_u_value",
            "main_heating_system_efficiency",
        ]
    ).describe()
    logger.info(b_desc)

    resi_types = buildings["residential_type"].unique()

    h_summary = (
        buildings.group_by(["residential_type", "main_heating_energy"])
        .agg(count=pl.col("dwelling_count").sum())
        .pivot("residential_type", index="main_heating_energy", values="count")
        .fill_null(0)
        .with_columns(total=sum([pl.col(r) for r in resi_types]))
        .sort(["total"], descending=True)
        .head(10)
    )
    logger.info(h_summary)
    d_summary = (
        buildings.group_by(["residential_type", "dhw_energy"])
        .agg(count=pl.col("dwelling_count").sum())
        .pivot("residential_type", index="dhw_energy", values="count")
        .fill_null(0)
        .with_columns(total=sum([pl.col(r) for r in resi_types]))
        .sort(["total"], descending=True)
        .head(10)
    )
    logger.info(d_summary)


@duration_logging
def run_building_inference(buildings, dwellings, parameters):
    """
    Selects the heating system from a building by taking the system present in the associated dwellings
    Sets the total living area for the building
    Args:
        buildings:
        dwellings:
        parameters:

    Returns:

    """
    diagnosis_data = parameters.diagnosis_data.with_columns(
        pl.col("heating_system").cast(pl.Categorical),
        pl.col("region").cast(pl.String).cast(pl.Int64),
    )
    dwelling_data = dwellings.group_by("building_id").agg(
        pl.col("heating_system").mode().first(),
        pl.col("living_area").sum(),
    )
    buildings = buildings.filter(pl.col("to_sim")).with_columns(
        pl.col("region").cast(pl.Int64)
    )
    logger.info(f"Residential buildings to simulate : {buildings.shape[0]}")
    buildings = buildings.join(
        dwelling_data, on="building_id", how="left", validate="1:1"
    )
    logger.info(
        f"Start of building energy performance inference by joining "
        f"energy performance diagnosis with buildings"
    )
    buildings = join_random_by_columns(
        buildings,
        diagnosis_data,
        BUILDING_JOIN_COLUMNS,
        join_quantile=parameters.diagnosis_join_quantile,
    )

    incompatible_dhw = (buildings["dhw_energy"] == "fossil_gas") & (
        buildings["gas_network_connection"] == False
    )
    buildings = buildings.with_columns(
        dhw_energy=pl.when(incompatible_dhw)
        .then(pl.col("main_heating_energy"))
        .otherwise(pl.col("dhw_energy"))
    )

    incompatible_backup = (buildings["backup_heating_energy"] == "fossil_gas") & (
        buildings["gas_network_connection"] == False
    )
    buildings = buildings.with_columns(
        backup_heating_energy=pl.when(incompatible_backup)
        .then(pl.lit("biomass"))
        .otherwise(pl.col("backup_heating_energy"))
    )

    log_summary(buildings)

    return buildings
