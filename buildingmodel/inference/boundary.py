from buildingmodel import data_path
import pandas as pd
import numpy as np
import polars as pl
from pathlib import Path
import random
from ..creation.boundary import EXTERIOR_WALL, INTERIOR_WALL, ROOF, FLOOR

from ..logger import duration_logging


def transfer_u_values(boundaries, buildings):
    boundary_u_values = buildings.unpivot(
        on=["wall_u_value", "roof_u_value", "floor_u_value"],
        index="building_id",
        variable_name="boundary_type",
        value_name="u_value",
    ).with_columns(boundary_type=pl.col("boundary_type").str.strip_suffix("_u_value"))

    boundary_dict = {
        EXTERIOR_WALL: "wall",
        INTERIOR_WALL: "wall",
        ROOF: "roof",
        FLOOR: "floor",
    }
    expr = pl.coalesce(
        pl.when(pl.col("type") == key).then(pl.lit(value))
        for key, value in boundary_dict.items()
    )
    boundaries = boundaries.with_columns(boundary_type=expr)

    boundaries = boundaries.join(boundary_u_values, on=["building_id", "boundary_type"])

    return boundaries


def transfer_window_data(boundaries, buildings):
    boundaries = boundaries.join(
        buildings.select(["building_id", "wall_window_share", "wall_window_u_value"]),
        on="building_id",
    ).rename(
        {
            "wall_window_share": "window_share",
            "wall_window_u_value": "window_u_value",
        }
    )
    boundaries = boundaries.with_columns(
        window_share=pl.when(pl.col("type") != EXTERIOR_WALL)
        .then(pl.lit(0.0))
        .otherwise(pl.col("window_share")),
        window_u_value=pl.when(pl.col("type") != EXTERIOR_WALL)
        .then(pl.lit(0.0))
        .otherwise(pl.col("window_u_value")),
    )

    boundaries = boundaries.with_columns(
        window_solar_factor=pl.when(pl.col("window_u_value") > 4.0)
        .then(pl.lit(0.85))
        .when(pl.col("window_u_value") > 3.0)
        .then(pl.lit(0.8))
        .when(pl.col("window_u_value") > 2.0)
        .then(pl.lit(0.75))
        .when(pl.col("window_u_value") > 1.0)
        .then(pl.lit(0.65))
        .otherwise(pl.lit(0.5))
    )

    return boundaries


@duration_logging
def run_boundary_inference(buildings, boundaries, parameters):
    """
    for each building, select the records in the diagnosis database that matches the most closely the building considering
    the construction year class, the residential type, the heating system and the district. Applies the relevant
    parameters from the diagnosis to the building and associated boundaries

    Args:
        buildings:
        boundaries:
        parameters:

    Returns:

    """

    boundaries = transfer_u_values(boundaries, buildings)
    boundaries = transfer_window_data(boundaries, buildings)

    return boundaries
