from polars.selectors import duration

from buildingmodel import data_path
from pathlib import Path
import numpy as np
import polars as pl
import geopandas as gpd

from buildingmodel.logger import duration_logging


def set_network_connection(buildings, parameters, census):
    """
    Defines which building are connected to the gas network using a logistic probability function of the distance to
    the gas network. The loc parameter of the logistic function correponds to the value at which the cumulative
    distribution is equal to 0.5.

    Args:
        buildings:
        parameters:

    Returns:

    """
    # np.random.seed()
    buildings = buildings.with_columns(
        gas_network_connection=pl.when(
            pl.col("has_network_city_level") & (~pl.col("has_network_grdf_data"))
        )
        .then(True)
        .otherwise(False)
    )

    district_list = buildings["district"].unique()
    construction_year_classes = (
        buildings["construction_year_class"].drop_nulls().unique()
    )
    residential_types = buildings["residential_type"].drop_nulls().unique()

    all_connected_ids = []

    for district in district_list:
        for residential_type in residential_types:
            for construction_year_class in construction_year_classes:
                building_selection = buildings.filter(
                    (pl.col("district") == district)
                    & (pl.col("residential_type").is_not_null())
                    & (pl.col("residential_type") == residential_type)
                    & (pl.col("has_network_city_level"))
                    & (pl.col("has_network_grdf_data"))
                    & (pl.col("construction_year_class").is_not_null())
                    & (pl.col("construction_year_class") == construction_year_class)
                )[["building_id", "dwelling_count", "gas_network_distance"]]

                census_dwellings = census.filter(
                    (pl.col("district") == district)
                    & (pl.col("residential_type") == residential_type)
                    & (pl.col("construction_year_class") == construction_year_class)
                )

                if (building_selection.shape[0] == 0) | (
                    census_dwellings.shape[0] == 0
                ):
                    continue

                total_dwellings = census_dwellings["ipondl"].sum()
                gas_dwellings = census_dwellings.filter(
                    pl.col("heating_system").cast(pl.String).str.contains("fossil gas")
                )["ipondl"].sum()

                gas_network_share = gas_dwellings / total_dwellings
                gas_network_share *= 1.0 + parameters.gas_network_margin
                gas_network_count = np.ceil(
                    building_selection["dwelling_count"].sum() * gas_network_share
                )

                building_selection = building_selection.sort(by="gas_network_distance")
                building_selection = building_selection.with_columns(
                    cumulative_dwelling_count=pl.col("dwelling_count").cum_sum()
                )

                if building_selection["dwelling_count"][0] > gas_network_count:
                    connected_ids = building_selection["building_id"][[0]]
                else:
                    connected_ids = building_selection.filter(
                        pl.col("cumulative_dwelling_count") <= gas_network_count
                    )["building_id"]

                all_connected_ids.extend(connected_ids)

    buildings = buildings.with_columns(
        gas_network_connection=pl.when(pl.col("building_id").is_in(all_connected_ids))
        .then(True)
        .otherwise(False)
    )

    return buildings


def calculate_distance_to_network(buildings, building_geometries, parameters):
    """
    Calculates the minimal distance between each building and the gas network route
    Args:
        buildings:

    Returns:

    """

    if (buildings["has_network_city_level"].sum() == 0.0) & (
        buildings["has_network_grdf_data"].sum() == 0.0
    ):
        buildings = buildings.with_columns(gas_network_distance=0.0)
    else:
        try:
            district_buffer = (
                building_geometries.geometry.union_all()
                .convex_hull.buffer(1.0e3)
                .bounds
            )
        except:
            print(buildings.district.unique())
        gas_network_route = gpd.read_file(
            Path(data_path["gis"]) / parameters.gas_network_route, bbox=district_buffer
        )

        # we obtain the nearest neighbour in the gas network route
        building_distances = building_geometries.sjoin_nearest(
            gas_network_route, how="left", distance_col="gas_network_distance"
        )

        # we remove duplicates when the distance to two network lines are exactly the same
        building_distances = pl.from_pandas(
            building_distances.drop_duplicates(subset=["building_id"])[
                ["gas_network_distance", "building_id"]
            ]
        )
        buildings = buildings.join(building_distances, on="building_id")

    return buildings


@duration_logging
def run_gas_network_inference(buildings, building_geometries, parameters, census):
    """
    Selects the heating system from a building by taking the system present in the associated dwellings
    Sets the total living area for the building
    Args:
        buildings:
        dwellings:
        parameters:

    Returns:

    """
    buildings = calculate_distance_to_network(
        buildings, building_geometries, parameters
    )
    buildings = set_network_connection(buildings, parameters, census)

    return buildings
