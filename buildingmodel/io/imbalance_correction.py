import numpy as np
import polars as pl
import logging

logger = logging.getLogger("buildingmodel")


def convert_residential_type(buildings, target_residential_type, convert_count):
    """
    This function converts the residential types of selected buildings

    Args:
        buildings:
        target_residential_type:
        convert_count:

    Returns:

    """

    original_residential_type = (
        "house" if target_residential_type == "apartment" else "house"
    )
    candidate_buildings = (
        buildings.filter(pl.col("residential_type") == original_residential_type)
        .sort(by=["dwelling_count", "floor_area"])
        .with_columns(dwelling_cumulative=pl.col("dwelling_count").cum_sum())
    )
    below_threshold = candidate_buildings.filter(
        pl.col("dwelling_cumulative") < convert_count
    )
    above_threshold = candidate_buildings.filter(
        pl.col("dwelling_cumulative") >= convert_count
    ).head(1)
    building_to_convert = pl.concat([below_threshold, above_threshold])

    if building_to_convert.shape[0] > 0:
        buildings = buildings.with_columns(
            residential_type=pl.when(
                pl.col("building_id").is_in(building_to_convert["building_id"])
            )
            .then(pl.lit(target_residential_type))
            .otherwise(pl.col("residential_type"))
        )

    return buildings


def convert_to_residential_use(buildings, residential_type, convert_count, parameters):
    height_bounds = (2.0, 10.0) if residential_type == "house" else (3.0, 100.0)
    area_bounds = (50.0, 500.0) if residential_type == "house" else (200.0, 5.0e3)

    candidate_buildings = buildings.filter(
        (pl.col("main_usage") == "unknown")
        & (
            (pl.col("secondary_usage") != "residential")
            | (pl.col("secondary_usage").is_null())
        )
        & (pl.col("height").is_between(*height_bounds))
        & (pl.col("floor_area").is_between(*area_bounds))
    ).with_columns(
        dwelling_count=pl.when(residential_type == "house")
        .then(1.0)
        .otherwise(
            (pl.col("floor_area") / parameters.minimal_apartment_floor_area).floor()
        )
    )

    if candidate_buildings["dwelling_count"].sum() < convert_count:
        building_to_convert = candidate_buildings
    else:
        candidate_buildings = candidate_buildings.sort(
            by=["dwelling_count", "floor_area"]
        ).with_columns(dwelling_cumulative=pl.col("dwelling_count").cum_sum())
        below_threshold = candidate_buildings.filter(
            pl.col("dwelling_cumulative") < convert_count
        )
        above_threshold = candidate_buildings.filter(
            pl.col("dwelling_cumulative") >= convert_count
        ).head(1)
        building_to_convert = pl.concat([below_threshold, above_threshold])

    building_ids_to_convert = building_to_convert["building_id"].to_list()
    created_dwellings = building_to_convert["dwelling_count"].sum()

    logger.info(
        f"Converting buildings with unknown usage to create {created_dwellings} dwellings ({residential_type}) out of {convert_count} missing from census data."
    )
    b_row_ids = buildings.with_row_index("index").filter(
        pl.col("building_id").is_in(building_ids_to_convert)
    )["index"]
    buildings = buildings.with_columns(
        residential_type=buildings["residential_type"]
        .cast(pl.String)
        .scatter(b_row_ids, residential_type)
        .cast(pl.Categorical)
    )
    buildings = buildings.with_columns(
        main_usage=buildings["main_usage"]
        .cast(pl.String)
        .scatter(b_row_ids, "residential")
        .cast(pl.Categorical)
    )
    buildings = buildings.with_columns(
        dwelling_count=buildings["dwelling_count"].scatter(
            b_row_ids, building_to_convert["dwelling_count"]
        )
    )

    return buildings


def convert_to_unknown_use(buildings, residential_type, convert_count):
    candidate_buildings = buildings.filter(
        (pl.col("residential_type") == residential_type)
    )

    if candidate_buildings["dwelling_count"].sum() < convert_count:
        building_to_convert = candidate_buildings["building_id"].to_list()
    else:
        building_to_convert = (
            candidate_buildings.sort(by=["dwelling_count", "floor_area"])
            .with_columns(dwelling_cumulative=pl.col("dwelling_count").cum_sum())
            .filter(pl.col("dwelling_cumulative") <= convert_count)
        )["building_id"].to_list()

    logger.info(
        f"Converting {len(building_to_convert)} residential buildings ({residential_type}) to unknown usage out of {convert_count} in excess from census data."
    )
    buildings = buildings.with_columns(
        pl.when(pl.col("building_id").is_in(building_to_convert))
        .then(
            pl.struct(
                residential_type=None, dwelling_count=0.0, main_usage=pl.lit("unknown")
            )
        )
        .otherwise(pl.struct(["residential_type", "dwelling_count", "main_usage"]))
        .struct.field(["residential_type", "dwelling_count", "main_usage"])
    )

    return buildings


def get_dwellings_by_type(input_df, dwelling_var):
    dwelling_counts = (
        input_df.drop_nulls("residential_type")
        .group_by("residential_type")
        .agg(pl.col(dwelling_var).sum())
    )

    dw_dict = {
        x["residential_type"]: x[dwelling_var] for x in dwelling_counts.to_dicts()
    }

    for r in ["apartment", "house"]:
        if r not in dw_dict.keys():
            dw_dict[r] = 0.0

    return dwelling_counts, dw_dict


def correct_dwelling_count(buildings, parameters, census):
    b_df, current_dwellings = get_dwellings_by_type(buildings, "dwelling_count")
    census_data_in_case = census.filter(
        pl.col("district").cast(pl.Categorical).is_in(buildings["district"].unique())
    )
    c_df, census_dwellings = get_dwellings_by_type(census_data_in_case, "ipondl")

    if c_df.shape[0] > 0:
        logger.info("Dwelling by type before correction")
        logger.info(
            c_df.join(
                b_df.filter(pl.col("residential_type") != "other"),
                on="residential_type",
            )
            .rename({"ipondl": "census", "dwelling_count": "building_data"})
            .sort("census")
        )

        for res_type in census_dwellings.keys():
            other_res_type = "apartment" if res_type == "house" else "house"
            if current_dwellings[res_type] < census_dwellings[res_type]:
                dw_to_add = census_dwellings[res_type] - current_dwellings[res_type]
                if current_dwellings[other_res_type] > census_dwellings[other_res_type]:
                    dw_to_convert = min(
                        current_dwellings[other_res_type]
                        - census_dwellings[other_res_type],
                        dw_to_add,
                    )
                    buildings = convert_residential_type(
                        buildings, res_type, dw_to_convert
                    )
                else:
                    dw_to_convert = 0.0

                dw_to_add -= dw_to_convert
                buildings = convert_to_residential_use(
                    buildings, res_type, dw_to_add, parameters
                )

        b_df, current_dwellings = get_dwellings_by_type(buildings, "dwelling_count")

        for res_type in census_dwellings.keys():
            if current_dwellings[res_type] > census_dwellings[res_type]:
                dw_to_remove = current_dwellings[res_type] - census_dwellings[res_type]
                buildings = convert_to_unknown_use(buildings, res_type, dw_to_remove)

        b_df, current_dwellings = get_dwellings_by_type(buildings, "dwelling_count")
        logger.info("Dwelling by type after correction")
        logger.info(
            c_df.join(
                b_df.filter(pl.col("residential_type") != "other"),
                on="residential_type",
            )
            .rename({"ipondl": "census", "dwelling_count": "building_data"})
            .sort("census")
        )
    else:
        logger.info(
            "No correction applied as no census data is available. Dwelling types below."
        )
        logger.info(
            (
                b_df.filter(pl.col("residential_type") != "other")
                .rename({"dwelling_count": "building_data"})
                .sort("building_data")
            )
        )

    return buildings


def remove_excess_apartments(apartment_buildings, parameters):
    apartment_buildings = apartment_buildings.with_columns(
        max_dwelling_count=(
            pl.col("floor_area") / parameters.minimal_apartment_floor_area
        ).floor()
    )
    mask = pl.col("dwelling_count") > pl.col("max_dwelling_count")
    excess_apartments = (
        apartment_buildings.filter(mask)
        .with_columns(excess=pl.col("dwelling_count") - pl.col("max_dwelling_count"))[
            "excess"
        ]
        .sum()
    )
    b_row_ids = apartment_buildings.with_row_index("index").filter(mask)["index"]
    apartment_buildings = apartment_buildings.with_columns(
        dwelling_count=apartment_buildings["dwelling_count"].scatter(
            b_row_ids, apartment_buildings.filter(mask)["max_dwelling_count"]
        )
    )
    logger.info(
        f"Removing {excess_apartments} dwellings from apartment buildings due to insufficient floor area."
    )
    apartment_buildings = apartment_buildings.with_columns(
        residential_type=pl.when(pl.col("dwelling_count") == 1)
        .then(pl.lit("house"))
        .otherwise(pl.col("residential_type")),
        main_usage=pl.when(pl.col("dwelling_count") == 0)
        .then(pl.lit("unknown"))
        .otherwise(pl.col("main_usage")),
    )

    return excess_apartments, apartment_buildings


def add_dwellings_to_apartment_buildings(apartment_buildings, excess_apartments):
    mask = pl.col("dwelling_count") < pl.col("max_dwelling_count")
    candidate_buildings = apartment_buildings.filter(mask)
    available_dwellings = candidate_buildings.with_columns(
        available=pl.col("max_dwelling_count") - pl.col("dwelling_count")
    )["available"].sum()

    if excess_apartments >= available_dwellings:
        selected_buildings = candidate_buildings.with_columns(
            available_dwelling=pl.col("max_dwelling_count") - pl.col("dwelling_count")
        )
    else:
        selected_buildings = (
            candidate_buildings.with_columns(
                available_dwelling=pl.col("max_dwelling_count")
                - pl.col("dwelling_count")
            )
            .sort(by=["available_dwelling"])
            .with_columns(dwelling_cumulative=pl.col("available_dwelling").cum_sum())
            .filter(pl.col("dwelling_cumulative") <= excess_apartments)
        )
    building_ids_to_convert = selected_buildings["building_id"].to_list()
    added_dwellings = selected_buildings["available_dwelling"].sum()

    logger.info(
        f"Adding {added_dwellings} dwellings to apartment buildings with available floor area."
    )
    b_row_ids = apartment_buildings.with_row_index("index").filter(
        pl.col("building_id").is_in(building_ids_to_convert)
    )["index"]
    apartment_buildings = apartment_buildings.with_columns(
        dwelling_count=apartment_buildings["dwelling_count"].scatter(
            b_row_ids, selected_buildings["max_dwelling_count"]
        )
    )

    return apartment_buildings, added_dwellings


def correct_dwelling_count_in_apartment_buildings(buildings, parameters):
    """

    Args:
        buildings:
        parameters:

    Returns:

    """
    apartment_buildings = buildings.filter(pl.col("residential_type") == "apartment")

    if apartment_buildings.shape[0] == 0:
        return buildings

    other_buildings = buildings.filter(
        (pl.col("residential_type") != "apartment")
        | (pl.col("residential_type").is_null())
    )
    dwelling_to_add, apartment_buildings = remove_excess_apartments(
        apartment_buildings, parameters
    )
    apartment_buildings, added_dwellings = add_dwellings_to_apartment_buildings(
        apartment_buildings, dwelling_to_add
    )
    dwelling_to_add -= added_dwellings

    other_buildings = convert_to_residential_use(
        other_buildings, "apartment", dwelling_to_add, parameters
    )

    buildings = pl.concat(
        [other_buildings, apartment_buildings.drop("max_dwelling_count")],
        how="vertical",
    )
    return buildings


def fill_null_construction_classes(buildings, census):
    census_data_in_case = census.filter(
        pl.col("district").is_in(buildings["district"].unique())
    )
    total_dwellings = census_data_in_case["ipondl"].sum()
    class_distribution = census_data_in_case.group_by("construction_year_class").agg(
        probability=pl.col("ipondl").sum() / total_dwellings
    )
    if class_distribution.shape[0] == 0:
        return buildings
    unknown_buildings = buildings.filter(pl.col("construction_year_class").is_null())
    sampled_classes = np.random.choice(
        class_distribution["construction_year_class"].to_list(),
        size=len(unknown_buildings),
        p=class_distribution["probability"].to_list(),
    )
    unknown_buildings = unknown_buildings.with_columns(
        construction_year_class=pl.Series(sampled_classes).cast(pl.Categorical)
    )
    buildings = pl.concat(
        [
            buildings.filter(pl.col("construction_year_class").is_not_null()),
            unknown_buildings.select(buildings.columns),
        ],
        how="vertical",
    )

    return buildings
