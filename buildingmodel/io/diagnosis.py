from buildingmodel import data_path
import polars as pl
from pathlib import Path


def load_diagnosis_data(parameters):
    """

    Args:
        parameters: instance of Parameters class containing simulation level parameters

    Returns:

    """

    parameters.diagnosis_data = pl.read_parquet(
        Path(data_path["diagnosis"]) / parameters.diagnosis_file
    )
