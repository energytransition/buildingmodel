import geopandas as gpd
from pathlib import Path
from buildingmodel import data_path
import warnings

building_data_folder_path = Path("####/bdtopo")


def load_buildings(district_id):
    """

    :param geo_boundary:
    :param level:
    :param columns:
    :return:
    """
    districts = gpd.read_file(
        Path(data_path["gis"]) / "districts_with_network_presence.gpkg",
        engine="pyogrio",
        where=f"district == {district_id}",
    )
    if districts.shape[0] == 0:
        return None
    geo_poly = districts["geometry"][0]
    geo_bounds = geo_poly.convex_hull.bounds

    building_path = (
        building_data_folder_path / f"BDTOPO_R{int(districts['region'][0])}.gpkg"
    )

    with warnings.catch_warnings():
        warnings.filterwarnings("ignore")
        building_gdf = gpd.read_file(
            building_path, layer="batiment", engine="pyogrio", bbox=geo_bounds
        )

    building_gdf["intersection_share"] = (
        building_gdf["geometry"].intersection(geo_poly).area
        / building_gdf["geometry"].area
    )
    building_gdf = building_gdf.loc[building_gdf.intersection_share >= 0.5].copy()
    building_gdf.drop(columns=["intersection_share"], inplace=True)

    return building_gdf
