# -*- coding: utf-8 -*-

from pvlib.iotools import read_epw

EPW_name_dict = {
    "temp_air": "air_temperature",
    "temp_dew": "dew_point_temperature",
    "dni": "direct_normal_radiation",
    "dhi": "diffuse_horizontal_radiation",
    "opaque_sky_cover": "opaque_sky_cover",
}


def load_data(file_path):
    """Reads an EPW climate file and returns the relevant climate data

    Args:
        file_path (str): path to EPW climate file

    Returns:
        Dataframe : DataFrame containing air_temperature, dew_point_temperature, wind_speed, wind_direction,
        direct_normal_radiation, diffuse_horizontal_radiation
    """

    data, metadata = read_epw(file_path, coerce_year="2018")
    data = data.loc[:, EPW_name_dict.keys()].copy()
    data = data.rename(EPW_name_dict, axis=1)
    data.astype(float)

    return data, metadata
