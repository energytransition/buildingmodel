import numpy as np


def plot_3D_district(
    buildings, boundaries, value_to_display, color_range=None
):  # pragma: no cover
    """Plots a 3D representation of the district in a notebook using k3d

    Args:
        buildings (GeoDataframe):  a GeoDataframe containing the building geometries and parameters
        boundaries (GeoDataframe): a GeoDataframe containing the boundary geometries
        value_to_display (str): the name of the variable to use to color the boundaries
        color_range (list of floats): a list of two floats that will be used as min and max values for the range of colors
        to be displayed. If set to None, the range is set to the min and max of the values to be displayed. Defaults to
        None.

    Returns:

    """

    try:
        import mapbox_earcut as earcut
        import k3d
    except ImportError:
        raise Warning(
            "To use the plot_3D_district, the following additional dependencies must be installed : mapbox_earcut and k3d "
        )
        return None

    if value_to_display in boundaries.columns:
        pass
    elif value_to_display in buildings.columns:
        boundaries[value_to_display] = 0.0
        for idx in buildings.index:
            boundaries.loc[boundaries["building_id"] == idx, value_to_display] = (
                buildings.loc[idx, value_to_display]
            )
    else:
        raise AttributeError(
            f"value_to_display ({value_to_display} can not be found in either the boundaries or the "
            f"buildings)"
        )

    plot = k3d.plot(grid_visible=False)
    mesh = np.zeros((0, 3))
    indices = np.zeros((0, 3))
    attr_values = np.zeros((0, 0))

    for idx in boundaries.index:
        attr_value = boundaries.loc[idx, value_to_display]
        boundary_type = boundaries.loc[idx].geometry.type
        if boundary_type == "LineString":
            xy = np.array(boundaries.loc[idx].geometry.xy)
            xy = np.tile(xy, 2)
            z = np.zeros(xy.shape[1])
            z[:2] = boundaries.loc[idx, "altitude"]
            z[2:] = boundaries.loc[idx, "altitude"] + boundaries.loc[idx, "height"]
            attr_values = np.append(attr_values, attr_value * np.ones(z.shape[0]))

            new_mesh = np.vstack([xy[0], xy[1], z]).T
            new_indices = np.array([[0, 2, 1], [2, 3, 1]]) + mesh.shape[0]
            mesh = np.vstack([mesh, new_mesh])
            indices = np.vstack([indices, new_indices])
        if boundary_type == "Polygon":
            xy = np.array(boundaries.loc[idx].geometry.exterior.coords.xy)
            xy = np.tile(xy, 2)
            z = np.ones(xy.shape[1]) * boundaries.loc[idx, "altitude"]
            attr_values = np.append(attr_values, attr_value * np.ones(z.shape[0]))
            new_mesh = np.vstack([xy[0], xy[1], z]).T
            rings = np.array([new_mesh.shape[0]])
            new_indices = (
                earcut.triangulate_float64(new_mesh[:, :2], rings) + mesh.shape[0]
            )
            new_indices = new_indices.reshape((-1, 3))
            mesh = np.vstack([mesh, new_mesh])
            indices = np.vstack([indices, new_indices])

    if color_range is None:
        color_range = [attr_values.min(), attr_values.max()]
    plt_mesh = k3d.mesh(
        mesh,
        indices,
        color_map=k3d.colormaps.basic_color_maps.Jet,
        attribute=attr_values,
        color_range=color_range,
        opacity=1.0,
        flat_shading=True,
        side="double",
    )
    plot += plt_mesh
    return plot
