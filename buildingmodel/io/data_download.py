from pathlib import Path
import requests
from tqdm import tqdm
from buildingmodel import data_path
import py7zr


def download_file(url, save_path):
    """
    Downloads a file from a given URL and saves it to the specified path with a progress bar.

    :param url: str, The URL of the file to download.
    :param save_path: str or Path, The full path (including filename) where the file should be saved.
    """
    save_path = Path(save_path)  # Ensure it's a Path object
    save_path.parent.mkdir(parents=True, exist_ok=True)  # Create directories if needed

    if save_path.exists():
        print(f"File {save_path.name} already exists. Skipping download.")

    try:
        with requests.get(url, stream=True, timeout=10) as response:
            response.raise_for_status()  # Raise an error for bad status codes
            total_size = int(response.headers.get("content-length", 0))

            # Use tqdm for a progress bar
            with (
                save_path.open("wb") as file,
                tqdm(
                    desc=save_path.name,
                    total=total_size,
                    unit="B",
                    unit_scale=True,
                    unit_divisor=1024,
                ) as bar,
            ):
                for chunk in response.iter_content(chunk_size=8192):
                    file.write(chunk)
                    bar.update(len(chunk))

        print(f"✅ Downloaded: {save_path}")

        if save_path.suffix == ".7z":
            extract_7z(save_path)

    except requests.exceptions.RequestException as e:
        print(f"❌ Failed to download {url}: {e}")


def extract_7z(archive_path):
    """
    Extracts a .7z archive in its destination folder.

    :param archive_path: Path, The .7z archive file.
    """
    extract_path = archive_path.parent  # Extract in the same folder
    try:
        with py7zr.SevenZipFile(archive_path, mode="r") as archive:
            archive.extractall(path=extract_path)
        print(f"📂 Extracted: {archive_path} → {extract_path}")
    except Exception as e:
        print(f"❌ Failed to extract {archive_path}: {e}")


if __name__ == "__main__":
    districts_link = "https://storage.googleapis.com/building-inference-data/districts_latest.parquet"
    census_link = (
        "https://storage.googleapis.com/building-inference-data/census_latest.parquet"
    )
    diagnosis_link = "https://storage.googleapis.com/building-inference-data/energy_performance_diagnosis_latest.parquet"
    gas_network_link = "https://storage.googleapis.com/building-inference-data/gas_network_route_latest.gpkg"
    weather_link = (
        "https://storage.googleapis.com/building-inference-data/FRA_2004-2018.7z"
    )

    # Example Usage
    files_to_download = {
        districts_link: Path(data_path["gis"]) / "districts.parquet",
        census_link: Path(data_path["census"]) / "census.parquet",
        diagnosis_link: Path(data_path["diagnosis"])
        / "energy_performance_diagnosis.parquet",
        gas_network_link: Path(data_path["gis"]) / "gas_network_route.gpkg",
        weather_link: Path(data_path["climate"]) / "FRA_2004-2018.7z",
    }

    for url, path in files_to_download.items():
        download_file(url, path)
