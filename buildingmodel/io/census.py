from buildingmodel import data_path
import polars as pl
from pathlib import Path


def load_census_in_case(department_list, parameters):
    """

    Args:
        parameters: instance of Parameters class containing simulation level parameters
        dwellings: a Dataframe containing dwellings parameters and results

    Returns:

    """
    return (
        pl.scan_parquet(Path(data_path["census"]) / parameters.census)
        .filter(pl.col("department").is_in(department_list))
        .filter(
            ~pl.col("occupancy_type").is_in(["vacant dwelling", "occasional housing"])
        )
        .with_columns(pl.col("ipondl").fill_nan(1.0))
        .with_columns(
            gas_network_connection=pl.col("heating_system")
            .cast(pl.String)
            .str.contains("fossil gas")
        )
    ).collect()
