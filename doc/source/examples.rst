.. _examples:

Examples
========
buildingmodel usage examples are provided as Ipython Notebooks. To view the results of a notebook, click on the link below. To
run the notebook, open a command line tool, activate the conda environment containing buildingmodel, go to the folder containing the
notebooks (buildingmodel/doc/notebooks) and run the command :code:`jupyter notebook`.


.. toctree::
   :glob:

   notebooks/*
