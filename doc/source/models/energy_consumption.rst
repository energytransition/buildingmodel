Energy consumption Models
===============
This section contains all the models that turn energy needs into energy consumption of the corresponding fuels.

.. automodule:: buildingmodel.models.energy_consumption
   :members:
