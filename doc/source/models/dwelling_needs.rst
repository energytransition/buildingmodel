Dwelling Need Models
===============
This section contains the models used to obtain energy needs linked to the occupant of a dwelling (specific energy use
and domestic hot water)

.. automodule:: buildingmodel.models.dwelling_needs
   :members:
