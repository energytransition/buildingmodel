Inference of missing parameters
===============================

.. toctree::
   :caption: Reference
   :maxdepth: 2

   inference/boundary
   inference/dwelling
   inference/heating_system
