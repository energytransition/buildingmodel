.. _installation:

Installation
============

Pre-requisites
--------------

* `Anaconda 3.8 distribution <https://www.anaconda.com/distribution/>`_ or `Miniconda3 distribution <https://docs.conda.io/en/latest/miniconda.html>`_
* To clone buildingmodel's Gitlab repository, `Git <https://git-scm.com/downloads>`_ (On Windows, `Git for Windows <https://git-for-windows.github.io/>`_ is preferred)
* If you wish to install from source, the cpp compiler corresponding to buildingmodel's python version is needed. On linux,
  ``sudo apt-get install -y gcc make g++``. On windows, follow the instructions at `WindowsCompiler https://wiki.python.org/moin/WindowsCompilers`
* A working docker installation. Detailed installation instructions for each platform available at `Docker install https://docs.docker.com/install/`
* A Gitlab account with at least guest level to install from source or at least reporter level to pull docker image

Installing from source
----------------------

Obtaining the source code
~~~~~~~~~~~~~~~~~~~~~~~~~

You can either :
    - directly download the source in a compressed folder from buildingmodel's main gitlab page : `https://gitlab.com/energytransition/buildingmodel`
    - clone buildingmodel's repository using git either with SSH
      ``git clone git@gitlab.com:energytransition/buildingmodel.git`` or with HTTPS
      ``git clone https://gitlab.com/energytransition/buildingmodel.git`` after having set up the corresponding
      authentication.


Creating the conda environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Open a command line tool in the buildingmodel root folder, enter the ``ci`` folder, and then run the following :

.. code-block:: shell

    conda env create -f conda_env.yml


For more information on conda environments, please visit https://conda.io/docs/using/envs.html .

.. warning::
    On Windows, if the environment installation fails for one the pip installed package, please make sure that the cpp
    compiler was properly installed and that the folder containing the executables is on the PATH.

.. note::
    If a conda environment with the name buildingmodel already exists, this will fail. In this case, it is possible to
    either remove the existing environment with ``conda remove --name buildingmodel --all`` or to change the name of
    the environment to be created by editing it directly in the .yml file.

Installing buildingmodel
~~~~~~~~~~~~~~~~~~~~~~~~

Activate the environment we have just created :

.. code-block:: shell

    conda activate buildingmodel

Then, from the root folder of buildingmodel, run :

.. code-block:: shell

    python setup.py install

Running the test suite
~~~~~~~~~~~~~~~~~~~~~~

From the root folder of buildingmodel, run :

.. code-block:: shell

    pytest


Obtaining complete census and diagnosis data
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


BuildingModel source code only contains sample census, district and diagnosis data necessary for testing purposes. To
obtain complete data, download them at :

- `census data (https://storage.googleapis.com/building-inference-data/district_level_census_latest.hdf)`_ to copy in buildingmodel/data/census/district_level_census.hdf
- `district data (https://storage.googleapis.com/building-inference-data/districts_latest.gpkg)`_ to copy in buildingmodel/data/gis/districts.gpkg
- `diagnosis data (https://storage.googleapis.com/building-inference-data/district_level_diagnosis_data_latest.hdf)`_ to copy in buildingmodel/data/diagnosis/district_level_diagnosis_data.hdf

Obtaining climate data
~~~~~~~~~~~~~~~~~~~~~~

BuildingModel relies on weather data designed for building thermal energy simulation produced by [onebuilding](http://climate.onebuilding.org/).
Users can download relevant weather data for their use case and copy it into buildingmodel/data/weather.



Compiling a local version of the documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Additional dependencies are needed to compile the documentation :
    - pandoc and graphviz. Can be installed on linux with ``sudo apt-get install pandoc graphviz graphviz-dev``. On
      windows, see installation instructions on their respective websites.
    - python packages listed in doc/requirements.txt

When the dependencies are installed, cd into the ``doc`` folder, and then run the following :

.. code-block:: shell

    make html

The resulting documentation can be accessed by opening the file ``doc\_build\html\index.html``


Using docker image
------------------

Login to buildingmodel's container registry using your Gitlab's username and password:

.. code-block:: shell

    docker login -u  {$USERNAME} -p {$PASSWORD} registry.gitlab.com/energytransition/buildingmodel


Pull the docker image you want to use :

.. code-block:: shell

    docker pull {$IMAGE_NAME}:{$TAG}

For example, the latest dev image :

.. code-block:: shell

    docker pull registry.gitlab.com/yassineabdelouadoud/buildingmodel/dev:latest

Or the latest master image :

.. code-block:: shell

    docker pull registry.gitlab.com/yassineabdelouadoud/buildingmodel:latest

For a complete list of images and tags, please visit `buildingmodel's container registry https://gitlab.com/energytransition/buildingmodel/container_registry`

You can then start a container in interactive mode while bind mounting a local directory containing your scripts and data :

.. code-block:: shell

    docker run -it --name={$CONTAINER_NAME} --mount type=bind,source={$SOURCE_DIRECTORY},target=/buildingmodel/{$TARGET_DIRECTORY} registry.gitlab.com/energytransition/buildingmodel/dev /bin/bash
    conda activate buildingmodel
    cd {$TARGET_DIRECTORY}
    python {$MY_SCRIPT_NAME}

If your script generates output files, you can retrieve them in the {$SOURCE_DIRECTORY}
