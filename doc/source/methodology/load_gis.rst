.. _load_gis:

Loading and processing building-level GIS data
==============================================

Buildingmodel relies extensively on the geopandas library to provide GIS functionalities, including :

 * Reading of GIS files
 * Storing, accessing and exporting GIS data in GeoDataFrame instances
 * Area calculations

Geometrical Model
-----------------
Each building is represented by a 2D polygon in the xy plane combined with an altitude and height to create the right
prism modelling the geometry of a building.

.. figure:: ../images/single_building_footprint.png
    :height: 200px

    An example of two polygons representing building footprints


.. figure:: ../images/building_3D.png
    :height: 200px

    The equivalent right prism representing the building geometries

To guarantee this, only the 2D coordinates of geometrical objects of types Polygon are kept (if the type if
MultiPolygon, the first Polygon is selected). Clockwise order coordinates are converted to counter-clockwise order.

Coordinate reference system
---------------------------
Calculations of areas in buildingmodel assume that GIS data is provided in a planar coordinate
reference system using meters as unit (Lambert93 in the case of France). If that is not the case, the Geopandas library
provides utilities to convert the coordinate reference system to a suitable one.

Renaming
--------
To allow for data processing independent of the data source, the columns used in buildingmodel are renamed.

.. csv-table:: Renaming conventions for BDTOPO V3
   :header: "original name", "buildingmodel"
   :widths: 20, 20

    'USAGE1', 'main_usage'
    'USAGE2', 'secondary_usage'
    'DATE_APP', 'construction_date'
    'NB_LOGTS', 'dwelling_count'
    'NB_ETAGES', 'floor_count'
    'MAT_MURS', 'wall_material'
    'MAT_TOITS', 'roof_material'
    'HAUTEUR', 'height'
    'Z_MIN_SOL', 'altitude_min'
    'Z_MAX_SOL', 'altitude_max'

For the same reason, values in main_usage and secondary_usage columns are also renamed.

.. csv-table:: Renaming conventions for BDTOPO V3 usages
   :header: "original name", "buildingmodel"
   :widths: 20, 20

    'Résidentiel', 'residential'
    'Agricole', 'agriculture'
    'Annexe', 'annex'
    'Commercial et services', 'commercial'
    'Indifférencié', 'unknown'
    'Industriel', 'industrial'
    'Religieux', 'religious'

Filtering
---------
Buildings can be removed from the simulation if they do not respect the following conditions :

 * at least minimal altitude is known
 * height is known and above a minimal value (default 2.5 meters)
 * footprint area is above a minimal value (default 15 square meters)

Classifying buildings
---------------------
To allow matching with census and energy diagnosis databases, buildings are classified using the following criteria :

 * construction year class (by default : before 1918, 1919-1945, 1946-1970, 1971-1990, 1991-2005, after 2006)
 * residential type (house or apartment, depending on the number dwelling in the building)
 * residential_only is set to True if the building has no other use
 * has_annex is set to True if secondary_usage is annex
 * the district in which the building resides, using a spatial join with GIS data of administrative boundaries


.. figure:: ../images/bdtopo_districts.png
    :height: 350px

    Spatial join of BDTOPO with IRIS (French administrative boundary)

Miscellaneous calculations
--------------------------
 * altitude is equal to altitude_min if altitude_max is unknown. Otherwise, it is the mean of altitude_min and
   altitude_max
 * if either height or floor_count is unknown, the other is used to obtain it (default floor height is 2.5 meters
 * if both height and floor_count are known, their coherence is checked using a minimal (default 2.5 meters) and maximal
   floor height (default 6 meters).
 * floor_area is the product of the footprint area by floor_count


