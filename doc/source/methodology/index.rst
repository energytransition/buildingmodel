
***********************
Methodology description
***********************

In this chapter, we will describe the methodology implemented in buildingmodel following the order of data processing :

 * data loading
 * object creation
 * missing data inferences
 * energy models
 * indicator calculation

..  toctree::

    data_sources
    load_gis
    boundary_creation
    dwelling_creation
    dwelling_inference
    building_inference
    climate_models
    solar_gains
    dwelling_needs
    thermal_losses
    thermal_needs
    energy_consumption
    energy_indicators

