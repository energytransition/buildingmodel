.. _boundary_creation:

Boundary Creation
=================
In accordance with the simplified geometrical model used, a building can have four types of boundaries :

 * Exterior walls
 * Interior walls (between buildings with vertical adjacency)
 * Roofs
 * Floors

In this section, we will describe the procedure used to create these boundaries and obtain the necessary characteristics.

Geometry simplification
-----------------------
The level of accuracy of GIS data can sometimes lead to an overly complex building geometry for the purposes of the
simplified thermal models used in buildingmodel. Keeping this level of accuracy would lead to unnecessary computation
as well as numerical errors due to potentially very small surfaces.

.. figure:: ../images/complex_building_footprint.png
    :height: 250px

    Example of building with unnecessary level of detail


Algorithm
~~~~~~~~~
This simplification is achieved using the Visvalingam-Whyatt (VW) line-simplification algorithm. It is an iterative
algorithm with the following procedure at each step :

 * Calculate the area of each triangle constituted by three consecutive points
 * Find the smallest triangle
 * If its area is below a tolerance, remove the middle point and start the next step
 * if not, the algorithm has terminated

.. figure:: ../images/line_simplification_vw.png
    :height: 150px

    Visual illustration of the VW algorithm (source: `<https://martinfleischmann.net/tag/simplification/>`_)

.. figure:: ../images/simplified_building_footprint.png
    :height: 250px

    Result of the VW algorithm for the previous building with a tolerance of 2 m²

The VW algorithm can be applied to each polygon independently only if there is no adjacency. In the presence of
adjacency, applying it independently would create artificial gaps between polygons. To avoid this, we first compute the
topology of the group of polygons (ie the list of arcs composing the polygons, including the shared arcs) and apply the
VW algorithm to each arc.

Parametric study of the tolerance parameter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The objective of this simplification is to limit the number of boundaries for which solar gains and thermal losses need
to be computed while limiting the loss of accuracy in terms of total area of boundaries. To verify that this is indeed
the case, we compute the number of boundaries as well as the total area for values of the tolerance ranging from 0 to
10 m² in increment of 0.5 m² for two test cases : a dense urban center and a rural town.

.. figure:: ../images/urban_center_test_case.png
    :height: 400px

    Footprints of the buildings in the dense urban center (city of Coulommiers)

Below we display the number of boundaries as well as the total area for values relative to the values for a tolerance of
0 (no simplification). We can observe that the number of boundaries decreases much faster than the total area (for
example, a tolerance of 6 m² reduces the number of boundaries by 25% while causing only a 2% loss of boundary area). We
propose the use of 2 m² as a default value to ensure the elimination of numerical errors related to small boundaries
with minimal area loss.

.. figure:: ../images/impact_of_VW_tolerance_urban.png
    :height: 600px

    Results for the urban test case

.. figure:: ../images/impact_of_VW_tolerance_rural.png
    :height: 600px

    Results for the rural test case

Boundary geometry
-----------------
The following geometrical attributes of boundaries are computed :

 * height (if a vertical boundary)
 * altitude
 * coordinates of the center
 * azimuth (if a vertical boundary)
 * area
 * projected xy geometry (polygon for horizontal boundaries, line for vertical)

Horizontal boundaries
~~~~~~~~~~~~~~~~~~~~~
Horizontal boundary geometries (roof and floors) are obtained directly from the building footprint allowing direct
computation of the boundary attributes.

Vertical boundaries
~~~~~~~~~~~~~~~~~~~
After the simplification algorithm has been applied, building footprints are represented by lists of arcs that can
belong to one or two buildings. When an arc belongs to only one building, an exterior wall is created for each edge of
the arc with the corresponding line as the projected xy geometry and the same height and altitude as the building. When
the arc belongs to two buildings, the heights and altitudes of the two buildings are compared to determine the exterior
and interior walls to create. The following configurations are possible :

 * building 0 and building 1 are disjointed (2 exterior walls)
 * building 0 is higher than building 1 with the same altitude  (1 exterior wall and 2 interior walls)
 * building 0 is higher than building 1 with a higher altitude  (1 exterior wall, 2 interior walls, 1 equivalent floor)
 * building 0 is contained by building 1  (1 exterior wall, 2 interior walls, 1 equivalent floor)

To avoid creating arbitrarily small walls, a minimal tolerance is applied when comparing the heights and altitudes of
the buildings (default value of 1 meter=. The configuration obtained defines the height and altitude of each wall,
while the projected xy geometry is the line from the corresponding edge.

.. figure:: ../images/boundary_creation_3d.png
    :height: 200px

    Example of 3D boundary geometry with exterior wall in blue, roofs in green and equivalent floor in yellow

Additional attributes
---------------------

 * The unique index representing a building is set on corresponding boundaries as 'building_id'
 * When the boundary is an interior wall, the usage of the other building is recorded as 'adjacency_usage'
 * After the boundary creation is complete the sum of all boundary areas is recorded at the building level by type of
   boundary