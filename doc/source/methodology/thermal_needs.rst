.. _thermal_needs:

Thermal Needs
=============
Thermal needs are obtained by combining the thermal losses and gains calculated :

.. math::
    Q^{annual} = \sum_{b \in B} Q_{b, i}^{annual} + Q_{v}^{annual} - Q_{solar}^{annual} - \sum_{dw \in DW} Q_{occupant, dw}^{annual}

where :math:`B` is the set of boundaries, :math:`Q_{solar}^{annual}` is the annual solar gains,  :math:`DW` is the set of
dwellings, :math:`Q_{occupant, dw}^{annual}`.

Heated area share
~~~~~~~~~~~~~~~~~
The preceding equation returns the heating needs if the whole building were heated. However, in practice, there are
situations where that is not the case (example : a garage in an individual house, garbage rooms in collective housing).
To model this effect, we define the heated area share as the share of a building that will be heated for residential use.
Then the heating gains are obtained using the preceding equation while multiplying the losses and gains linked to
building geometry by the heated area share :

.. math::
    Q^{annual} = (\sum_{b \in B} Q_{b, i}^{annual} + Q_{v}^{annual} - Q_{solar}^{annual}) \times HAS - \sum_{dw \in DW} Q_{occupant, dw}^{annual}

where :math:`HAS` is the heated area share.

The heated area share is calculated by multiplying the living area share by a coefficient and clipping the resulting
values to minimal and maximal values dependent on 'residential_type', 'has_annex' and 'residential_only'.

.. csv-table:: Heated area share
   :header: "residential type", "residential only", "has annex", "coefficient", "min", "max"
   :widths: 20, 20, 20, 20, 20, 20

    house, True, False, 1, 1, 1
    house, True, True, 1.2, 0.2, 1
    house, False, True/False, 1.2, 0.0, 1
    apartment, True, False, 1, 1, 1
    apartment, True, True, 1.3, 0.6, 1
    apartment, False, True/False, 1.2, 0.0, 1