.. _dwelling_needs:

Occupant Related Energy Needs
=============================
Dwelling occupants generate energy needs related to their behavior which can be grouped in three categories :

 * domestic hot water needs
 * cooking
 * specific electricity use (refrigerator, dishwashing, television, etc.)

In turn, part of the energy consumption related to these uses will be turned into heat which, together with the body
heat produced by occupants, will contribute to the building heating needs.

Two modes of simulation are executed with differing objectives :

 * conventional mode : occupants follow an identical and conventional behavior defined in energy diagnosis norms,
   independent of the type of occupancy, so that the resulting energy consumption can be used to determine the
   building energy class
 * actual mode : occupant follow a stochastic behavior dependent on the occupancy type, so that that the resulting
   energy consumption can be compared to energy meter data

Domestic hot water needs
~~~~~~~~~~~~~~~~~~~~~~~~
In conventional mode, the domestic hot water needs are obtained as a function of the conventional occupant count, itself
a function of the living area using the model defined in the 3CL DPE method. Details are available p.68 of the
`3CL DPE methodology <http://www.rt-batiment.fr/IMG/pdf/annexe_methode_de_calcul_3cl-dpe_v1.3.pdf>`_

In actual mode, the daily domestic hot water use by occupant in liters is drawn in a beta distribution with parameters
2.5 and 4.5 and scaled to lie in the interval 10 liters to 150 liters. This distribution has been selected to fit the
data from a national study of domestic hot water consumption.

.. figure:: ../images/dhw_need.png
    :height: 200px

    Histogram of measured domestic hot water needs by occupant

The energy needs associated to the hot water consumption are then obtained by making the hypotheses that the water
needs to be heated from the ground temperature to 40°C. Annual needs are obtained using the average ground temperature,
while peak needs rely on the minimal ground temperature.

Specific electricity use
~~~~~~~~~~~~~~~~~~~~~~~~
Specific use is not taken into account in conventional energy models as it is unrelated to building characteristics. In
actual mode, the annual specific use by occupant is drawn in a beta distribution depending on the 'residential_type' :

.. csv-table:: Specific use beta distribution
   :header: "residential type", "min (in W)", "max (in W)", "a", "b"
   :widths: 20, 20, 20, 20, 20

    house, 900, 3000, 2.5, 4.5
    apartment, 700, 2600, 2.0, 4.5

The maximal specific use is selected depending on the number of occupant and residential type of the dwelling

.. csv-table:: Specific use maximal power (in W)
   :header: "residential type", "1", "2", "3", "4", "5", "6"
   :widths: 20, 20, 20, 20, 20, 20, 20

    house, 6000, 6000, 6000, 6000, 9000, 9000
    apartment, 3000, 3000, 6000, 6000, 6000, 9000

Cooking (experimental)
~~~~~~~~~~~~~~~~~~~~~~
Cooking is the only significant specific use of energy that can be delivered by fuels other than electricity (mainly
gas). However, we currently lack open data to determine which dwelling use gas a cooking fuel. To work around this issue,
we currently use the fuel of the main heating system to define a probability of using gas for cooking and then affect a
share of the specific uses to the gas consumption accordingly.

Attenuation with occupant count
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Annual domestic hot water and specific energy use by occupant decrease with the number of occupants in a dwelling. To
approximate this, we use a fictitious number of occupants :

.. math::
    FOC = OC^{1 \over 1.2}

where :math:`FOC` is the fictitious occupant count and :math:`OC` is the real occupant count.

Occupant gains
~~~~~~~~~~~~~~
Occupant-related heat gains group together heat gains due to occupant body heat and occupant use of energy in the
dwelling.
In conventional mode, occupant-related heat gains are taken to be 70 Wh by day and by m² of living area.
In actual mode, occupant-related heat gains are the sum of metabolic gains (average of 25 W by occupant) and
40% of the specific needs.


Impact of occupancy type
~~~~~~~~~~~~~~~~~~~~~~~~
In actual mode, the annual domestic hot water and specific need are multiplied by an intermittency factor that
represents the share of time occupants will be in the dwelling and that is drawn in a beta distribution whose parameters
depends on the occupancy type (except for vacant dwelling where it is 0) :

.. csv-table:: Intermittency factor beta distribution
   :header: "occupancy type", "min", "max", "a", "b"
   :widths: 20, 20, 20, 20, 20

    occasional housing, 0.1, 0.4, 2.5, 4.5
    second home, 0.05, 0.25, 2.5, 4.5
    house primary residence, 0.75, 1.0, 4.5, 2.5
    apartment primary residence, 0.65, 0.95, 4.5, 2.5

In conventional mode, the annual domestic hot water needs are multiplied by an intermittency factor that depends on the
residential type

.. csv-table:: Conventional intermittency factor
   :header: "residential type", "intermittency factor"
   :widths: 20, 20

    house, 0.95
    apartment, 0.85
