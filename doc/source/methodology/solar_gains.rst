.. _solar_gains:

Solar Gains
===========
Solar gains corresponds to the solar energy received by the building that will contribute to lowering the heating needs
of the building. They are estimated in the following manner for each boundary (except floors) :
 * Calculation of solar masks
 * Calculation of direct radiation received
 * Calculation of radiation transmitted by the windows
 * Calculation of the share contributing to heating needs

.. note::  Diffuse radiation is currently ignored as the significant increase in computation cost associated with complex
           sky models is not justified by the relatively small contribution of diffuse radiation to heating needs.

Solar masks
~~~~~~~~~~~
A solar mask is an angular representation of the environment of a given boundary. For each azimuth interval, the value
recorded corresponds to the highest angular height observed from the point of view of the center of the boundary. This
is obtained using the following procedure :
 * discretizing the polygons representing the building roofs
 * Calculate the azimuth and height of each point from the point of view of the boundary center
 * For each azimuth interval, select the highest height from the subset of corresponding points

An azimuth resolution of 10° and a grid resolution of 2 meters have been set as default values, representing an
acceptable compromise between computation costs and accuracy.

Direct radiation
~~~~~~~~~~~~~~~~
For each boundary, the height and azimuth of the sun are compared with the mask values to determine at which time steps
the boundary receives direct radiation. When that is the case, the direct radiation is calculated by projecting the
normal radiation on the plane of the boundary.

Transmitted radiation
~~~~~~~~~~~~~~~~~~~~~
To obtain the transmitted radiation, the area of windows of the boundary is calculated using the 'window_share' and
'area' attributes. The share that is transmitted after reflection at the surface of the window if obtained using the
following equation :

.. math::
    (1 - {AOI \over 90})^5 \times TF

where :math:`AOI` is the angle of incidence of the sun on the window in degrees and :math:`TF` is the window
transmission factor.

Heating need contribution
~~~~~~~~~~~~~~~~~~~~~~~~~
Using the simulation-level parameters 'heating_season_start' and 'heating_season_end' together with building-level
attribute 'heating_set_point', we determine the time steps during the heating season for which the exterior temperature
is below the set point, as an approximation of the occurrence of heating needs. We then calculate the total solar gains
as the sum of the transmitted radiation during these time steps.