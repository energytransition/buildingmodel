.. _dwelling_creation:

Dwelling Creation
=================
Dwellings are created according to the 'dwelling_count' value of each building. At the creation of the dwelling, the
following attributes are inherited from the corresponding building :
 * 'construction_year_class'
 * 'residential_type'
 * 'building_id'
 * 'floor_area_share' (the ratio of the building 'floor_area' by its 'dwelling_count')
 * 'residential_only'
 * 'has_annex'
