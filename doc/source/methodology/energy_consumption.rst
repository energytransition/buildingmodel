.. _energy_consumption:

Energy Consumption
==================
Energy consumption for the various fuels (electricity, oil, gas, biomass, district network) are obtained using the
energy needs calculated in the preceding sections (heating, domestic hot water, specific use), the efficiency of the
energy system and the fuel used.

Heating
~~~~~~~
Heating systems modelling takes into account the possibility of a backup heating system. The heating needs of the
building are split between the main heating system and the backup heating system using the 'backup_heating_share'
attribute. Then the energy consumption are obtained by dividing the heating needs by the corresponding efficiency
('main_heating_system_efficiency' and 'backup_heating_system_efficiency') and added to the corresponding fuel using
the 'main_heating_energy' and 'backup_heating_energy' attributes.

Domestic hot water
~~~~~~~~~~~~~~~~~~
Domestic hot water energy consumptions are calculated using standard efficiencies depending on the fuel used and then
added to the corresponding fuel consumption using the 'dhw_energy' attribute

.. csv-table:: DHW efficiencies
   :header: "fuel", "efficiency"
   :widths: 20, 20

    electricity, 0.8
    gas, 0.6
    oil, 0.5
    biomass, 0.6
    district network, 0.6

Specific use
~~~~~~~~~~~~
Specific use energy needs are already expressed in terms of final energy consumption so we simply add them to the
corresponding fuel (electricity except for gas cooking).