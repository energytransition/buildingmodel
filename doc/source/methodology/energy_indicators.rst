.. _energy_indicators:

Energy Indicators
==================
Energy indicators are calculated to facilitate the comparison of building energy performances in conventional
and actual mode, with both the total value and the value by living area returned.

Final energy consumption
~~~~~~~~~~~~~~~~~~~~~~~~
Final energy consumption is the sum of energy consumption for all the fuels considered.

Primary energy consumption
~~~~~~~~~~~~~~~~~~~~~~~~~~
Primary energy consumption is the sum of energy consumption for all the fuels considered using the following conversion
factors :

.. csv-table:: DHW efficiencies
   :header: "fuel", "efficiency"
   :widths: 20, 20

    electricity, 2.58
    gas, 1
    oil, 1
    biomass, 0.6
    district network, 0.6

Energy diagnosis class
~~~~~~~~~~~~~~~~~~~~~~
The energy diagnosis class is obtained by comparing the conventional primary consumption by living area to a set of
limits defined below :

.. csv-table:: Energy classes
   :header: "class", "limits"
   :widths: 20, 20

    A, < 50 kWh/m²
    B, 50 < ... < 90 kWh/m²
    C, 90 < ... < 150 kWh/m²
    D, 150 < ... < 230 kWh/m²
    E, 230 < ... < 330 kWh/m²
    F, 330 < ... < 450 kWh/m²
    G, > 450 kWh/m²

Application
~~~~~~~~~~~
We use the "Peuple-Boivin-St-Jacques" district of Saint-Etienne to illustrate the results obtained by the energy
indicator calculation.

.. figure:: ../images/energy_class.png
    :height: 450px

    Energy class for buildings of district "Peuple-Boivin-St-Jacques, Saint-Etienne".

