.. _building_inference:

Building and Boundary Inference
===============================
The process of inference aims at enriching GIS records by matching them with records from non-localized data sources
containing more complete information. In the case of building inference, we use the 'construction_year_class',
'residential_type', 'district' and 'heating_system' derived from GIS and census data to find the corresponding records
in the energy diagnosis data from which we extract the following attributes :
 * 'air_change_rate': the share of interior air that is exchanged with the exterior due to ventilation each hour (in vol/h)
 * 'main_heating_energy' : (possible values : 'electricity', 'gas', 'oil', 'biomass')
 * 'main_heating_system_efficiency' : the total efficiency of the main heating system (including generation, distribution
   and emission)
 * 'backup_heating_energy' : (possible values : 'electricity', 'gas', 'oil', 'biomass', None)
 * 'backup_heating_system_efficiency' : the total efficiency of the backup heating system (including generation,
   distribution and emission)
 * 'heating_system' : the main heating system of the dwelling (possible values : 'electric_heater', 'electric_heat_pump',
   'oil_boiler', 'gas_boiler', 'wood_boiler', 'district_network')
 * 'backup_heating_share' : the share of the heating needs that is provided by the backup system if one is present
 * 'dhw_energy' : the energy used for domestic hot water production
 * 'u_value': for roofs, floors, interior and exterior walls
 * 'window_share': for roofs and exterior walls
 * 'window_u_value': for roofs and exterior walls
 * 'window_solar_factor': the share of solar gains that are transmitted, for roofs and exterior walls

Algorithm
~~~~~~~~~
 1) All the buildings are grouped according to 'construction_year_class', 'residential_type', 'heating_system' and
    'district'
 2) The records with the same set of attributes in the energy diagnosis database are selected
 3) If there are fewer records than a predefined threshold, the operation is repeated by replacing 'district' with a
    higher geographic level ('city', 'city_group', 'department', 'region') until the threshold is attained. The
    threshold is the product of the number of dwellings in the group of building by simulation-level parameter
    'minimal_diagnosis_share' that is set to 10% by default.
 4) To deal with the paucity of diagnosis data for older buildings, a last resort is to remove the
    'construction_year_class' from the list of matching attributes if records cannot be found otherwise
 6) The matching diagnosis records are selected by random sampling with replacement
 7) The inferred data is transferred from the diagnosis records to the buildings and boundaries
 8) The geographic level of inference is recorded as 'inference_geo_level' attribute of buildings

Application
~~~~~~~~~~~
We use the "Peuple-Boivin-St-Jacques" district of Saint-Etienne to illustrate the results obtained by the building
inference algorithm.

.. figure:: ../images/wall_u_value_results.png
    :height: 450px

    Wall U values by construction year class for buildings of district "Peuple-Boivin-St-Jacques, Saint-Etienne".

.. figure:: ../images/heating_efficiency_results.png
    :height: 450px

    Main heating system efficiencies for buildings of district "Peuple-Boivin-St-Jacques, Saint-Etienne".
