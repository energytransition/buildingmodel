.. _climate_models:

Climate Models
==============
Climate models are used to estimate physical conditions that are not generally available in weather data sets. In the
case of buildingmodel, the following are calculated at each one hour step :

 * Height and azimuth of the sun using astronomical models with time and latitude/longitude as input
 * Sky temperature using the `EnergyPlus model <https://bigladdersoftware.com/epx/docs/8-3/engineering-reference/climate-calculations.html#energyplus-sky-temperature-calculation/>`_
 * Ground temperature using the `Kusuda model <https://bigladdersoftware.com/epx/docs/8-4/engineering-reference/undisturbed-ground-temperature-model-kusuda.html/>`_


