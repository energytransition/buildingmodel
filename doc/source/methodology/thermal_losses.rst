.. _thermal_losses:

Thermal Losses
==============
Thermal losses in buildings occur through two phenomenon :

 * conductive losses through boundaries (walls, roof, floor, windows)
 * convective losses due to ventilation and infiltration

The figure below illustrates a typical share of the various loss sources.

.. figure:: ../images/thermal_losses.png
    :height: 200px

    Thermal loss sources in buildings

In conventional mode, the heating set point is set to 19°C, while in actual mode it is a parameter with a default value
of 20°C.

Boundary Losses
~~~~~~~~~~~~~~~
The equation below models the annual losses through boundaries in steady state. At each time step of the heating season, the
exterior temperature is compared to the heating set point. If it is below, the loss is calculated as the difference
between the two temperatures multiplied by the product of the boundary U-value and area. If not, it is set to 0.

.. math::
    Q_{b, i}^{annual} = \sum_{\substack{t \in HS\\T_{ext}(t) < T_{sp}}} (T_{sp} - T_{ext}(t)) \times U_{b, i} \times A_{b,i} \times AF_{b, i}

where :math:`Q_{b, i}` is the loss for boundary :math:`i`, :math:`T_{ext}(t)` is the exterior temperature at step
:math:`t`, :math:`T_{sp}` is the heating set point, :math:`U_{b, i}` is the boundary U-value in W/(m²K) and
:math:`A_{b,i}` is the boundary area. :math:`AF_{b, i}` is the adjacency factor used to model the decrease in losses
when the boundary is adjacent to another building. It takes the following values :

 * 1 when the boundary is a roof, floor or exterior wall
 * 0.2 when the boundary is an interior wall and the adjacent building's use is residential
 * 0.8 when the boundary is an interior wall and the adjacent building's use is commercial
 * 1 when the boundary is an interior wall and the adjacent building's use is not residential nor commercial

The maximal losses are the equal to the losses at the step where the exterior temperature is minimal :

.. math::
    Q_{b, i}^{max} = (T_{sp} - T_{ext}^{min}) \times U_{b, i} \times A_{b,i} \times AF_{b, i}


Ventilation losses
~~~~~~~~~~~~~~~~~~
The equation below models the ventilation losses in steady state. At each time step of the heating season, the
exterior temperature is compared to the heating set point. If it is below, the loss is calculated as the difference
between the two temperatures multiplied by the product of the volume of exchanged air during the time step and the air
thermal capacity. If not, it is set to 0.

.. math::
    Q_{v}^{annual} = \sum_{\substack{t \in HS\\T_{ext}(t) < T_{sp}}} (T_{ext}(t) - T_{sp}) \times \frac{c_{air}}{3.6} \times ACR \times V

where :math:`c_{air}` is the air volumetric thermal capacity, :math:`ACR` is the building air change rate in %/h and
:math:`V` is the building volume.

The maximal losses are the equal to the losses at the step where the exterior temperature is minimal :

.. math::
    Q_{v}^{max} = (T_{sp} - T_{ext}^{min}) \times \frac{c_{air}}{3.6} \times ACR \times V


Thermal bridge losses (experimental)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Thermal bridge losses are a specific type of boundary losses that occur are at the intersection of boundaries, where
discontinuities of insulation can be present.

.. figure:: ../images/thermal_bridges.jpg
    :height: 200px

    Thermal bridges captured by thermal imagery of building (source: InfraTec)

Thermal bridge losses are expressed as a function of the length of the thermal bridge :

.. math::
    Q_{br, i}^{annual} = \sum_{\substack{t \in HS\\T_{ext}(t) < T_{sp}}} (T_{sp} - T_{ext}(t)) \times B_{br, i} \times L_{br,i}

where :math:`B_{br, i}` is the linear bridge thermal transmittance in W/(m.K) and :math:`L_{br,i}` is the length of the
linear thermal bridge in meters. While the thermal transmittance of boundary can be easily calculated from the characteristics
of the materials, thermal bridge transmittance is more complex to estimate as its depends on the specific way the
boundaries intersect, in particular the relative position of the insulation layer. As these information could not be
inferred from available data, we use a simplified approach to model thermal bridges, with only two possible values
of transmittance for each type of bridge considered (wall/roof, wall/floor, wall/intermediate floor), depending on the
insulation of the boundaries involved :

.. csv-table:: Thermal bridge transmittance
   :header: "thermal bridge", "insulated", "uninsulated"
   :widths: 20, 20, 20

    wall/roof, 0.2, 0.5
    wall/floor, 0.3, 0.6
    wall/intermediate floor, 0.3, 0.9

Impact of occupancy type
~~~~~~~~~~~~~~~~~~~~~~~~
To take into account the fact that occupants are not always present in the dwelling, the thermal losses are multiplied
by the same intermittency factors defined in section 1.9.5.

Application
~~~~~~~~~~~
We use the "Peuple-Boivin-St-Jacques" district of Saint-Etienne to illustrate the results obtained by the thermal losses
calculation.

.. figure:: ../images/ventilation_losses.png
    :height: 450px

    Ventilation losses by m² of living area of district "Peuple-Boivin-St-Jacques, Saint-Etienne".

.. figure:: ../images/total_losses.png
    :height: 450px

    Total thermal losses by m² of living area district "Peuple-Boivin-St-Jacques, Saint-Etienne".


.. figure:: ../images/thermal_loss_by_area_results.png
    :height: 450px

    Total thermal losses by m² of living area for each construction year class of buildings in district
    "Peuple-Boivin-St-Jacques, Saint-Etienne".