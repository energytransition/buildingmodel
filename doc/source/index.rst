.. buildingmodel documentation master file, created by
   sphinx-quickstart on Thu Sep  3 16:13:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to buildingmodel's documentation
========================================
Buildingmodel is a multi building energy simulation tool that takes as input GIS data describing the building geometries
and relevant properties (envelop thermal performance, energy systems, use profile) and returns the yearly energy
consumption and peak power. To achieve this, it runs the following sequence of models: detection of adjacencies between
buildings, calculation of boundary geometries, solar masks, climate data, solar gains, 3CL based thermal losses
modelling and energy system efficiency.

..  toctree::

    installation
    how_to_use
    examples
    methodology/index
    use_cases/index
    main
    io
    models
    reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
