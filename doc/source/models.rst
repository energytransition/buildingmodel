Models
======

.. toctree::
   :caption: Reference
   :maxdepth: 2

   models/climate
   models/solar_masks
   models/solar_gains
   models/thermal_losses
   models/thermal_needs
   models/dwelling_needs
   models/energy_consumption
