.. _overestimation:

Energy Consumption Overestimation
=================================
In the overall energy consumption analysis, we have identified that the more rural the case, the higher the
overestimation of energy consumption (no overestimation for Saint Etienne use case, slight overestimation for the
"Roannais" case and high one for the "Ambert-Livradois-Forez" case). To investigate this further, we look at
of energy consumption at the city level in the "Roannais" as a function of the share of individual houses
(:numref:`_overestimation_house_share`). While the separation between urban and rural and its impact on overestimation
is obvious, the share of houses alone is not a good predictor of overestimation as it can still range from +20% to
+100% with house shares above 90%.

.. _overestimation_house_share:
.. figure:: ../images/overestimation_house_share.png
    :height: 400px

    Overestimation of energy consumption at the city level as a function of house share, use case "Roannais"

In an effort to find a better predictor of overestimation of energy consumption, we consider the poverty level as the
next candidate, as buildingmodel does not yet take into account fuel poverty effects that could lead to effective heating
set points lower than the 20°C used as default value. While median income data is available for each city, poverty
levels are not, so we first train a 3rd-degree polynomial model of the relationship between the two to obtain poverty
level data for all cities.

.. figure:: ../images/median_income.png
    :height: 400px

    Poverty levels vs median income

We can then create a synthetic indicator which is the product of the house share by the poverty level and display below
its relationship with the overestimation of energy consumption for cities of more than 500 dwellings.

.. figure:: ../images/final_overestimation.png
    :height: 400px

    Overestimation of energy consumption at the city level as a function of house share multiplied by poverty levels, use case "Roannais"

Thus we can hypothesise that fine tuning parameters affecting individual houses and apartments separately (such as min
and max height of floors or ratio between living area and heated area) can be a potential solution to eliminate
the baseline overestimation of around 20% linked to individual houses while taking into account fuel poverty effects
seems necessary to account for the larger overestimations.