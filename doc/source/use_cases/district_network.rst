.. _district_network:

District Network Underestimation
================================
In the Saint-Etienne use case, the average buildingmodel district network consumption was 46 GWh, while the aggregated
network operator data was 84 GWh. Three possible explanations are considered :

 * the dwelling inference algorithm introduces a bias in the share of dwelling with 'district_network' as heating system
 * the building and boundary inference algorithm introduces a bias increasing the building envelop performance
 * the building and boundary inference algorithm introduces a bias in heating system efficiency

:numref:`_census_vs_sim` displays the share of each heating system in the census data and buildingmodel results. While
small differences can be observed for the gas boiler and electric heater systems, the district network shares are
almost identical with 5.6% in buildingmodel and 5.7% in the census data, disproving the first explanation.

.. _census_vs_sim:
.. figure:: ../images/district_network_census_vs_buildingmodel.png
    :height: 250px

    Inference geographic levels, use case Saint-Etienne

Out of more than 5 million energy diagnosis records used in the building and boundary inference, only 341 have district
network as heating system, leading to relatively higher inference geographic levels for such buildings, ie pairing them
with energy diagnosis records that are less relevant. As an illustration, in the Saint-Etienne case, no building with
district network has an inference level below region, while most of the others have inference levels below city group.
While the pairing may not be the most relevant, no significant bias in building envelop performance that could
explain such a significant decrease in consumption is present in the dataset, as shown in :numref:`_dn_u_values`.

.. _dn_u_values:
.. figure:: ../images/dn_u_values.png
    :height: 250px

    U value distributions for buildings with or without district network

In buildingmodel the heating system efficiency is supposed to include the generation, distribution, emission and
regulation efficiencies. However, when looking at the distribution of efficiencies for energy diagnosis with district
network as heating system (:numref:`_d_n_efficiency`), it becomes apparent that this is not the case for this subset of
diagnosis, as the values observed lie in the range expected for the combination of distribution, emission and
regulation efficiencies. As a consequence, buildingmodel simulates the energy consumption for the district network at
the foot of the building while the energy data corresponds to fuel used in the district network boilers. This could
explain the 45% underestimation of consumption, as losses in old, badly insulated district network can easily attain
15 to 20%, while boiler efficiencies could be as low as 70%. One avenue to confirm this hypotheses and eventually
remedy the current issue would be te use
`open data on district networks <https://www.statistiques.developpement-durable.gouv.fr/donnees-locales-de-consommation-denergie>`_
which contain both fuel consumption and delivered energy for each district network in France to derive a local district
network efficiency that could then be used in buildingmodel

.. _d_n_efficiency:
.. figure:: ../images/d_n_efficiency.png
    :height: 250px

    Distribution of district network heating system efficiency
