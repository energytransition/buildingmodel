
*********
Use Cases
*********


..  toctree::

    introduction
    use_case_characteristics
    total_energy_consumption
    variability
    district_network
    energy_overestimation
    energy_performance
    conclusion


