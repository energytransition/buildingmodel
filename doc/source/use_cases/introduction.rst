.. _introduction:


Introduction
============
In this chapter, we will present the results of three large scale buildingmodel use cases along with exogenous energy
consumption data and analyze them with the following objectives :

* validate the relevance of the methodology
* identify areas of improvement
* illustrate potential applications
Three areas have been selected to represent diverse building stocks :

* Saint-Etienne : a dense urban area of mostly primary residences
* "Roannais Agglomération" city group : a mixed of rural and urban areas of mostly primary residences
* "Ambert-Livradois-Forez" city group : a rural area with a high share of second homes

Energy data sources
~~~~~~~~~~~~~~~~~~~
The energy consumption data used is taken from a dataset made available by
`"Auvergne Rhône-Alpes Energie Environnement" <https://www.auvergnerhonealpes-ee.fr>`_, as part of the
`TerriStory project <https://auvergnerhonealpes.terristory.fr/>`_, an online platform displaying localized city-level
energy and climate-related emission data. Energy consumption is segmented by sector (residential, industry, agriculture, etc.), fuel and usage (heating,
cooling, domestic hot water, etc.), see
`here <https://www.orcae-auvergne-rhone-alpes.fr/fileadmin/user_upload/mediatheque/ORCAE/Documents/Publications/ORCAE_Methodologie_globale.pdf>`_
for the methodology used to generate the dataset.
These energy data come mainly from two types of sources :

* Metering data aggregated by distribution network operators for electricity, district heating and network gas
* Regional polls of energy retailers de-aggregated using census and climate data for other fuels : oil, biomass,
  liquefied natural gas

While the first type of data sources can suffer from inaccuracies due to, for example, inconsistent metering periods or
wrong classification of usage, it is generally much more reliable than the second source. By comparing buildingmodel
results to these energy consumption data for diverse building stocks, we aim at both validating the methodology
employed and identifying areas of improvements by correlating the differences observed with building characteristics.

Data preparation
~~~~~~~~~~~~~~~~
For each case, the following steps are performed to prepare data before the simulation :

* the list of districts is computed using administrative boundary data
* for each district, the corresponding department-level BDTOPO file is loaded and the buildings are selected using a
  spatial join with administrative boundary data
* for each district, the residential type and construction year classes are adjusted to match district-level census
  data (see below)
* for each district, the resulting GIS file is stored in a dedicated folder


.. note::  GIS building data are generated from cadastre and tax filing data, while census data is derived from
           questionnaires answered by inhabitants, so differences in, for example, building construction years can be
           expected. When substantial, these differences will lead to bias in census matching and eventually to
           potentially incorrect share of heating systems or occupancy types. To prevent this, we adjust the
           construction year classes and residential types of the GIS building data to match census data. This is done
           by iteratively transferring buildings from overrepresented classes to underrepresented classes until the
           difference reaches below a relative criterion.

Simulation setup
~~~~~~~~~~~~~~~~
For each district of each case :

* Data loading and object creation is performed once
* Inference and models are performed 10 times in parallel
* results for buildings, boundaries and dwellings are stored in the same folder as the GIS input data