.. _total_energy_consumption:


Overall energy consumption
==========================
We visualize below the simulations results as boxplots of energy consumption by fuels, with dashed lines representing
the exogenous energy consumption data.

.. figure:: ../images/case_summary_consumption_distribution_roannais.png
    :height: 400px

    Residential energy consumption by fuel, use case "Roannais"


.. figure:: ../images/case_summary_consumption_distribution_saint_etienne.png
    :height: 400px

    Residential energy consumption by fuel, use case Saint-Etienne

.. figure:: ../images/case_summary_consumption_distribution_ambert.png
    :height: 400px

    Residential energy consumption by fuel, use case "Ambert-Livradois-Forez"

We can make the following qualitative observations from these overall results :

 * the share of each fuel in the overall consumption obtained by buildingmodel match fairly well with energy data,
   except for district network, which is always underestimated
 * the variability of results between runs is rather low, with relative standard deviations in the low single-digit
 * The results of the Saint-Etienne case match well with observed energy data in absolute value
 * The results of the "Roannais" case present a consistent overestimation of energy consumption
 * The results of the "Ambert-Livradois-Forez" case present a consistent overestimation of energy consumption, in
   particular with respect to biomass
