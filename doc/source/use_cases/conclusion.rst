.. _conclusion:


Conclusion
==========
We have presented the results of large use cases of buildingmodel simulations compared with real-world energy data.
While the overall results are satisfactory, we have delved into specific issues present in the results and have managed
to identify potential avenues for improvement to correct them :

 * better modelling of district network losses by using localized district network open data
 * further investigation of variability levels, in particular concerning the effect of the minimal share of diagnosis, is
   needed
 * fine tuning of individual houses and apartment parameters to mitigate baseline overestimation
 * adding fuel poverty effects to occupant behavior modelling
