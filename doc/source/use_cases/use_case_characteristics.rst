.. _use_case_characteristics:


Use Case Characteristics
========================
In this section, we will describe the three uses cases by drawing on the data sources used as inputs of buildingmodel to
illustrate the diversity of the territories considered.

Population density
~~~~~~~~~~~~~~~~~~
Population density can be estimated at the district-level by matching census population data with administrative limit
GIS data. While the three use cases correspond to similar numbers of administrative districts, they vary significantly
in terms of land areas and population, as illustrated in Figure 1 and 2.

.. csv-table:: Number of districts
   :header: "Use Case", "Districts"

    Ambert-Livradois-Forez, 59
    Roannais, 63
    Saint Etienne, 78

.. figure:: ../images/population_by_case.png
    :height: 200px

    Legal population obtained from INSEE census data

.. figure:: ../images/area_by_case.png
    :height: 200px

    Land area calculated from INSEE administrative boundary data

As a consequence, the population densities at the district level can differ by 4 orders of magnitude between the use
cases, with the following average properties by use cases :

 * Saint Etienne is a very dense territory with an average density of 2070 inh./km²
 * "CC Ambert-Livradois-Forez" is a very sparse territory with an average density of 22 inh./km²
 * "CC Roannais" is a mix of dense and sparse territories with an average density of 140 inh./km²


.. figure:: ../images/population_density_distribution.png
    :height: 400px

    Distribution of population density by district for each use case (logarithmic scale)

Residential type
~~~~~~~~~~~~~~~~
the stark differences in population densities naturally lead to contrasted shares of individual houses and apartments in
the dwelling stocks with rural "Ambert-Livradois-Forez" having more than 90% of individual houses while the situation
is reversed in dense urban Saint-Etienne and balanced (55% houses, 45% apartments) in mixed rural/urban "Roannais".

.. figure:: ../images/dwellings_by_residential_type.png
    :height: 275px

    Share of dwellings for each residential type and each case


Construction periods
~~~~~~~~~~~~~~~~~~~~
Construction periods of the dwellings obtained from census data show the disparities between the territories, with
a majority of dwellings in Saint-Etienne built in the two periods after 1945, while more than half of the buildings
in "Ambert-Livradois-Forez" were built before 1918.

.. figure:: ../images/dwellings_by_construction_year.png
    :height: 400px

    Share of dwellings in each construction year class for each case


Occupancy
~~~~~~~~~
The type of occupancy of the dwelling is a major factor in its energy consumption. In that respect, the
"Ambert-Livradois-Forez" use case is remarkable with its 30% second home share.

.. figure:: ../images/dwellings_by_occupancy.png
    :height: 400px

    Share of dwellings for each type of occupancy and each case


Heating systems
~~~~~~~~~~~~~~~
Using the correct mix of heating systems is essential to allow fuel-by-fuel comparison of buildingmodel results with
energy consumption data. Here again, the contrasts between the use cases stem from the intrinsic differences in the
territories studied, with old rural individual houses in "Ambert-Livradois-Forez" equipped mostly of wood and oil
boiler, while post-WW2 collective housing buildings in Saint-Etienne are equipped mostly of gas boilers.

.. figure:: ../images/dwellings_by_heating_system.png
    :height: 400px

    Share of dwellings for each heating system and each case

Energy consumption
~~~~~~~~~~~~~~~~~~
Below in an overview of the energy consumption by fuel for the residential sector in the three use cases.

.. figure:: ../images/energy_data.png
    :height: 400px

    Energy consumption by fuel