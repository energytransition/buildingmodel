.. _energy_performance:


Building energy performance
===========================
In this section, we will consider the conventional building performance results as expressed by the energy diagnosis
class attributed to the building after the simulation.


Use case level
~~~~~~~~~~~~~~
We present results at the use case levels, with total counts of dwellings in each energy class for each use case.

.. figure:: ../images/energy_class_roannais.png
    :height: 250px

    Number of dwellings by energy class, use case "Roannais"


.. figure:: ../images/energy_class_saint_etienne.png
    :height: 250px

    Number of dwellings by energy class, use case Saint-Etienne

.. figure:: ../images/energy_class_ambert.png
    :height: 250px

    Number of dwellings by energy class, use case "Ambert-Livradois-Forez"

The "Roannais" and Saint-Etienne use case share a profile similar to the national average, while the "Ambert-Livradois-Forez" case presents a
profile skewed towards low performance buildings, which could be explained by the higher share of older buildings in
the territory. To assess this possibility, we display below the number of dwellings for each energy class and each
construction year class.

.. figure:: ../images/energy_class_ambert_by_age.png
    :height: 300px

    Number of dwellings by energy class and construction year class, use case "Ambert-Livradois-Forez"

While, indeed, the dwellings in the construction year class 'before 1918' explain a large part of the skewed
distribution towards low energy performance, the class '1971-1990' also displays such a profile, hinting at the
possibility of other factors specific to the use case. The most obvious considering the specificities of the
"Ambert-Livradois-Forez" case would be a bias in the models linked to the residential type. However, when we display
the energy classes by residential type as in :numref:`_energy_class_ambert_by_type`, we can observe that dwellings
of type 'apartment' also do not follow
a distribution close the national average. Moreover, when we display the same results for the "Roannais" case, which has
a balanced mix of houses and apartments, we observe a similar profile for both  :numref:`_energy_class_roannais_by_type`
(Houses are slightly less performant in general, but this is expected as, in general, houses have higher boundary areas
by square meter of living area). Additional investigation are necessary

.. _energy_class_ambert_by_type:
.. figure:: ../images/energy_class_ambert_by_type.png
    :height: 300px

    Number of dwellings by energy class and residential type, use case "Ambert-Livradois-Forez"

.. _energy_class_roannais_by_type:
.. figure:: ../images/energy_class_roannais_by_type.png
    :height: 300px

    Number of dwellings by energy class and residential type, use case "Roannais"

District level
~~~~~~~~~~~~~~
The buildingmodel methodology offers the possibility to go to the desired geographic level to gain greater insights on
the building stock characteristics. Below we present maps of districts with the color representing the share of very
low performance dwellings (class F and G).


.. figure:: ../images/energy_class_saint_etienne_map.png
    :height: 300px

    Share of very low performance dwellings (class F and G), use case Saint-Etienne

.. figure:: ../images/energy_class_roannais_map.png
    :height: 300px

    Share of very low performance dwellings (class F and G), use case "Roannais"
