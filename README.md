# BuildingModel

**Description**:  BuildingModel is a district-level building energy simulation tool designed to leverage the availability
of large scale open datasets on building geometry, census-derived dwelling characteristics and energy performance diagnosis.
The methodology proposed combines GIS data processing with statistical inference techniques to obtain a 
description of each building's envelop geometry (including adjacency), thermal performances (envelop and systems) 
and occupation. This description is then used as the input of a physical model calculating solar gains, losses through
boundaries and ventilation, heating system consumption and occupant-related energy usage.

  - **Technology stack**: Python 3, GeoPandas
  - **Status**:  Beta (0.2). The complete toolchain is functional, but validation and fine-tuning of the models are still ongoing.

**Screenshot**: 
Example of results that can be obtained from a BuildingModel run with the GeoPandas integration allowing easy interactive plotting in Jupyter Notebooks

![](./doc/source/images/energy_by_floor_surface.png)

**Documentation**:
The complete documentation for buildingmodel (methodology and API) is available at [DOCS](https://energytransition.gitlab.io/buildingmodel/)

## Limitations
BuildingModel has been developed and tested only in the French context, for which GIS, census and energy
diagnosis data is available. However, the generic character of the methodology should make it suitable for other 
countries as long as similar datasets can be obtained.
In addition, BuildingModel currently deals only with residential buildings, due to limitations on the availability of data for other building usages.

## Dependencies

BuildingModel depends mainly on Python 3 (currently tested on Python 3.8) and GeoPandas. A detailed set of dependencies
is available in [requirements.txt](./ci/requirements.txt)

## Installation

Detailed installation instructions available at [INSTALL](INSTALL.md).

## Usage

In its most basic form, BuildingModel can be used by invoking the high level `run_all` function and passing it the
path to the building GIS file :

```python
from buildingmodel.main import run_all, Parameters
from buildingmodel import data_path
from pathlib import Path

parameters = Parameters()
buildings, boundaries, climate, dwellings = run_all(Path(data_path['gis']) / 'testing' / 'test_sample.gpkg',
                                            None, parameters)
```

The results returned ar GeoDataFrame in the case of buildings and boundaries and DataFrame in the case of the climate
and dwellings. More advanced uses and result processing are available as jupyter notebook examples in the documentation.

## How to test the software

From the root folder of BuildingModel, run :

```
pytest
```

## Known issues

- Current occupant-related energy usage models are very crude and subject significant future changes. 
- Energy consumption of heating systems relies on a constant efficiency model that will be replaced by technology-specific models (in particular
for heat pumps). 
- Peak consumption estimates are provided by building but cannot be summed directly to obtain the 
  total peak consumption of the district as the peaks may not be all synchronous.
- Consumption of cooling systems is not modelled

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

BuildingModel is looking for users to provide feedback and bug reports on the initial set of functionalities as well as
developers to contribute to the next versions, with a focus on validation of models, cooling need simulation,
adaptation to other countries' datasets and building usages.

Instructions on how to contribute are available at [CONTRIBUTING](CONTRIBUTING.md).


## Open source licensing info
1. [LICENSE](LICENSE)

----

## Credits and references
BuildingModel physical models are directly derived from the work of Antoine Rogeau in [
Vers une approche intégrée d’aide à la planification énergétique territoriale : application à la rénovation énergétique des bâtiments](https://www.theses.fr/2020UPSLM014)

### Funding :

### Main contributors : 
- Antoine Rogeau
- [Robin Girard](https://www.minesparis.psl.eu/Services/Annuaire/robin-girard) -- [gitHub](https://github.com/robingirard) [LinkedIn](https://www.linkedin.com/in/robin-girard-a88baa4/) [google Scholar](https://scholar.google.fr/citations?user=cEYGStIAAAAJ&hl=fr)
- Seddik Yassine Abdelouadoud
