## [0.2.0] - 2021-03-22
### Added
- Inference :
  - boundaries : intermediary geographic levels between district and country have been introduced to allow more fine-grained
    geographic matching of diagnosis data. The new levels are : city, city group (EPCI in France), department and region.
- IO :
  - change in default date format for gis data on building construction date
  - automated detection of district using administrative boundary GIS file
- Models:
  - added specific need results at the building level
- General :
  - addition of LICENCE.md, CONTRIBUTING.md and INSTALL.md
## [0.1.0] - 2021-01-28
### Added
- IO :
  - Climate : load .epw climate files. Automatic selection of epw file closest to simulated buildings if no path to climate file provided
  - GIS : load of GIS files using geopandas (all extensions supported). Automatic cleaning and processing of BDTOPO-like data to obtain building geometry (footprint, height, altitude) and description.
- Creation of objects :
  - boundaries : simplification of building geometry. Detection of adjacency. Solar mask calculation.
  - dwellings : creation of dwellings using parameters obtained from GIS files
- Inference :
  - dwellings : heating system, number of occupants and living area are obtained from local census data
  - boundaries : the thermal parameters of boundaries (U values, window share, etc.) are obtained from local diagnosis
    data 
  - heating system and ventilation are obtained from local diagnosis data
- Models (for peak and annual values) :
  - climate : calculation of ground temperature from air temperature and sun position
  - solar gains : calculation solar energy transmitted to the building through windows
  - thermal losses through boundaries and ventilation
  - calculation of total thermal needs considering losses and gains
  - occupant-linked energy need : domestic hot water and specific electricity consumption
  - calculation of total energy consumption from energy needs and heating system characteristics
- Utilities :
  - buildingmodel.main.run_all : complete run from io to models
  - buildingmodel.main.run_multiple_inference : a single run of io and object creation followed by several runs of 
    inference and models.
  - buildingmodel.main.run_parallel_inference : a single run of io and object creation followed by several runs of 
    inference and models in parallel using parameters.n_cpus cores.
    
